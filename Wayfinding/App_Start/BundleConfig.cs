﻿using System.Web.Optimization;

namespace Wayfinding
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/core").Include(
                "~/Scripts/jquery-{version}.js",
                "~/EditorClient/pathfinder.js"));

            bundles.Add(new StyleBundle("~/Content/core").Include(
              //  "~/Content/Site.css"
              "~/EditorClient/style/style.css"
            ));

        }
    }
}