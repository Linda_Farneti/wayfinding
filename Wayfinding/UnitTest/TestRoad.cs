﻿using EntityModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wayfinding.Controllers.WebApi;

namespace Wayfinding.UnitTest
{
    [TestFixture]
    public class TestRoad
    {
        private System.Data.Entity.DbSet<EntityModels.Node> nodes;
        private List<Node> MockNodes;
        public System.Data.Entity.DbSet<EntityModels.Road> roads;
        List<Road> MockRoads;
        private TestManager TM;
        private IDbRoadController controller;
        private bool M;

        public TestRoad()
        {
            TM = TestManager.GetInstance();
            controller = TM.RoadController;
            roads = new WayFindingUniBOEntities1().Road;
            nodes = new WayFindingUniBOEntities1().Node;
            MockNodes = MockDatabase.GetInstance().Node;
            MockRoads = MockDatabase.GetInstance().Road;
            M = TM.Mock;
        }

        private int CountRoads()
        {
            return M ? MockRoads.Count() : roads.Count();
        }

    
        private void ClearDatabase()
        {
            if (!M)
            {
                TM.ClearDatabase();
            }
            /*else
            {
                MockRoads = new List<Road>();
            }*/
        }

        [TestCase]
        public void AddRoad()
        {
            ViewModels.AddRoad road = TM.CreateRoad();
            controller.AddRoad(road);
            Road r = M ? MockRoads.Where(a => a.ID == MockRoads.Select(x => x.ID).ToList().Max()).First() :
                         roads.Where(a => a.ID == roads.Select(x => x.ID).ToList().Max()).First();
            Assert.AreEqual(r.Length, Consts.ROAD_DEFAULT_LENGTH);
            Assert.AreEqual(r.RoadSize, Consts.ROAD_DEFAULT_SIZE);
            Assert.AreEqual(road.StartNodeID, r.StartNodeID);
            Assert.AreEqual(road.EndNodeID, r.EndNodeID);
            ClearDatabase();
        }

         [TestCase]
         public void RemoveRoad()
         {
            int TotalRoads;
            int[] nodesID = M ? MockNodes.Select(x => x.ID).ToArray() :
                                nodes.Select(x => x.ID).ToArray();
            int[] added;

            for (int i = 0; i < Consts.TO_ADD; i++)
            {
                controller.AddRoad(TM.CreateRoad());
            }
            TotalRoads = CountRoads();
            added = M? MockRoads.Where(x => x.Map == Consts.MAP_TEST_ID).Select(y => y.ID).ToArray() :
                       roads.Where(x => x.Map == Consts.MAP_TEST_ID).Select(y => y.ID).ToArray();
            for (int i = 0; i < Consts.TO_REMOVE; i++)
            {
                controller.RemoveRoad(added[i]);
            }
            Assert.AreEqual(CountRoads(), TotalRoads - Consts.TO_REMOVE);
            ClearDatabase();
         }

         [TestCase]
         public void UpdateRoad()
         {
             int ID = 0;
             ViewModels.AddRoad road = TM.CreateRoad();
             controller.AddRoad(road);
             ID = M ? MockRoads.Select(x => x.ID).ToList().Max() :
                      roads.Select(x => x.ID).ToList().Max();
             controller.UpdateRoad(new ViewModels.UpdateRoad { ID = ID, NewLength = 30, NewSize = 20});
             Road r = M ? MockRoads.Where(x => x.ID == ID).First() :
                          roads.Where(x => x.ID == ID).First();
             Assert.AreEqual(r.Length, 30);
            // Assert.AreEqual(r.RoadSize, 20);                             
             ClearDatabase();
         }
    }
}