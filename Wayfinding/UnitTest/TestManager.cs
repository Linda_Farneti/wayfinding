﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;
using Wayfinding.Controllers.WebApi;
using Wayfinding.ViewModels;
using Wayfinding.ViewModels.Node;

namespace Wayfinding.UnitTest
{
    public class TestManager
    {
        public System.Data.Entity.DbSet<EntityModels.Node> nodes = new WayFindingUniBOEntities1().Node;
        public System.Data.Entity.DbSet<EntityModels.Road> roads = new WayFindingUniBOEntities1().Road;
        public System.Data.Entity.DbSet<EntityModels.PointOfInterest> points = new WayFindingUniBOEntities1().PointOfInterest;
        public IDbNodeController NodeController;
        public IDbRoadController RoadController;
        public IDbPointController PointController;

        public WayFindingUniBOEntities1 Database;
        public MockDatabase DatabaseMock;
        private Random rand;
        public static TestManager instance;
        public bool Mock = false;

        private TestManager()
        {
            this.Setup();
            DatabaseMock = MockDatabase.GetInstance();
            Database = new WayFindingUniBOEntities1();
            rand = new Random();
        }

        /*public bool checkMock()
        {
            return 
        }*/

        private void Setup()
        {
            var container = new UnityContainer();
            Mock = System.Configuration.ConfigurationManager.AppSettings["UseMock"].Equals("True");
            if (Mock)
            {
                container.RegisterType<IDbNodeController, MockNodeController>();
                container.RegisterType<IDbRoadController, MockRoadController>();
                container.RegisterType<IDbPointController, MockPointController>();
            }
            else
            {
                container.RegisterType<IDbNodeController, NodeController>();
                container.RegisterType<IDbRoadController, RoadController>();
                container.RegisterType<IDbPointController, PointController>();
            }
            NodeController = container.Resolve<IDbNodeController>();
            RoadController = container.Resolve<IDbRoadController>();
            PointController = container.Resolve<IDbPointController>();
        }

        public AddNode CreateNode()
        {
            float lat, lng;
            AddNode node;
            lat = Randomize(50);
            lng = Randomize(50);
            node = new AddNode { Latitude = lat, Longitude = lng, Map = Consts.MAP_TEST_ID };
            return node;
        }

        public ViewModels.AddRoad CreateRoad()
        {
            int start, end;
            int[] nodesID = Database.Node.Select(x => x.ID).ToArray();
            start = nodesID[Randomize(nodesID.Count())];
            end = nodesID[Randomize(nodesID.Count())];
            return new ViewModels.AddRoad { Length = Consts.ROAD_DEFAULT_LENGTH, StartNodeID = start, EndNodeID = end, Size = Consts.ROAD_DEFAULT_SIZE, Map = Consts.MAP_TEST_ID };
        }

        public ViewModels.AddRoad CreateRoad(int StartNodeID, int EndNodeID)
        {
            ViewModels.AddRoad road = CreateRoad();
            road.StartNodeID = StartNodeID;
            road.EndNodeID = EndNodeID;
            return road;
        }

        public ViewModels.AddPoint CreatePoint()
        {
            int node;
            int[] nodesID = Mock ? DatabaseMock.Node.Select(x => x.ID).ToArray() : Database.Node.Select(x => x.ID).ToArray();
            node = nodesID[Randomize(nodesID.Count())];
            return new ViewModels.AddPoint {Description="description", Icon = "~/Images/campocalcio.svg", Map = Consts.MAP_TEST_ID, Name = "test", NodeID = node, Size = Consts.POINT_DEFAULT_SIZE };
        }

        public void ClearDatabase()
        {
            var NodesID = nodes.Where(x => x.Map == Consts.MAP_TEST_ID).ToList();
            var RoadsID = roads.Where(x => x.Map == Consts.MAP_TEST_ID).ToList();
            var IconsID = points.Where(x => x.Map == Consts.MAP_TEST_ID).ToList();

            foreach (PointOfInterest p in IconsID)
            {
                PointController.RemovePoint(p.NodeID);
            }

            foreach (Road r in RoadsID)
            {
                RoadController.RemoveRoad(r.ID);
            }

            foreach (Node n in NodesID)
            {
                NodeController.RemoveNode(n.ID);
            }
            Database.SaveChanges();
        }

        public static TestManager GetInstance()
        {
            if(instance == null)
            {
                instance = new TestManager();
            }
            return instance;
        }

        public int Randomize(int max)
        {
            return rand.Next(max);
        }
    }
}