﻿using EntityModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wayfinding.Controllers.WebApi;
using Wayfinding.Models;
using Wayfinding.Services;
using Wayfinding.ViewModels;

// ID : 88

namespace Wayfinding.UnitTest
{
    [TestFixture]
    public class TestGraph
    {
        public System.Data.Entity.DbSet<EntityModels.Node> nodes;
        public System.Data.Entity.DbSet<EntityModels.Road> roads;
        private IDbNodeController NodeController;
        private IDbRoadController RoadController;
        private List<Node> MockNodes;
        private List<Road> MockRoads;
        NodeQueryService nqs;
        private TestManager TM;
        private bool M;

        public TestGraph()
        {
            TM = TestManager.GetInstance();
            NodeController = TM.NodeController;
            RoadController = TM.RoadController;
            nodes = new WayFindingUniBOEntities1().Node;
            roads = new WayFindingUniBOEntities1().Road;
            MockNodes = MockDatabase.GetInstance().Node;
            MockRoads = MockDatabase.GetInstance().Road;
            nqs = new NodeQueryService();
            M = TM.Mock;
        }

        private void ClearDatabase()
        {
            if (!M)
            {
                TM.ClearDatabase();
            }
        }

       [TestCase]
        public void CheckNeighbours()
        {
            if (!M)
            {
                int m_ID, n_ID;
                List<int> neighbours;
                ViewModels.AddNode master, n;
                ViewModels.AddRoad road;
                NodeQueryService nqs = new NodeQueryService();

                master = TM.CreateNode();
                NodeController.AddNode(master);
                m_ID = nodes.Select(y => y.ID).Max();

                for (int i = 0; i < Consts.DEFAULT_NEIGHBOURS; i++)
                {
                    n = TM.CreateNode();
                    NodeController.AddNode(n);
                    n_ID = nodes.Select(y => y.ID).Max();
                    road = TM.CreateRoad(m_ID, n_ID);
                    RoadController.AddRoad(road);
                }

                neighbours = nqs.GetNeighbours(m_ID, Consts.MAP_TEST_ID);
                Assert.AreEqual(Consts.DEFAULT_NEIGHBOURS, neighbours.Count());

                for (int i = 0; i < Consts.NEIGHBOURS_TO_REMOVE; i++)
                {
                    int r = TM.Randomize(neighbours.Count());
                    int id = neighbours.ElementAt(r);
                    neighbours.RemoveAt(r);
                    NodeController.RemoveNode(id);
                }
                neighbours = nqs.GetNeighbours(m_ID, Consts.MAP_TEST_ID);
                Assert.AreEqual(Consts.DEFAULT_NEIGHBOURS - Consts.NEIGHBOURS_TO_REMOVE, neighbours.Count());
                ClearDatabase();
            }
        }
        
        [TestCase]
        public void CheckRoads()
        {
            int m_ID, n_ID;
            ViewModels.AddNode master, n;
            ViewModels.AddRoad road;
            master = TM.CreateNode();
            NodeController.AddNode(master);
            m_ID = M ? MockNodes.Select(y => y.ID).Max() : nodes.Select(y => y.ID).Max();
            Assert.AreEqual(0 , MockRoads.Where(r => r.Map == Consts.MAP_TEST_ID).Count());
            for (int i = 0; i < Consts.DEFAULT_NEIGHBOURS; i++)
            {
                n = TM.CreateNode();
                NodeController.AddNode(n);
                n_ID = M ? MockNodes.Select(y => y.ID).Max() : nodes.Select(y => y.ID).Max();
                road = TM.CreateRoad(m_ID, n_ID);
                RoadController.AddRoad(road);
            }
            Assert.AreEqual(Consts.DEFAULT_NEIGHBOURS, M ? MockRoads.Where(r => r.Map == Consts.MAP_TEST_ID).Count() :
                                                           roads.Where(r => r.Map == Consts.MAP_TEST_ID).Count());
            ClearDatabase();
        }

        /*[TestCase]
        public void CheckNearest()
        {
            ViewModels.AddNode nearest, farthest, master;
            ViewModels.AddRoad FirstRoad, SecondRoad;
            int m_ID, n_ID, f_ID, r1_ID, r2_ID;
            
            nearest = TM.CreateNode();
            NodeController.AddNode(nearest);
            n_ID = nodes.Select(y => y.ID).Max();

            farthest = TM.CreateNode();
            NodeController.AddNode(farthest);
            f_ID = nodes.Select(y => y.ID).Max();

            master = TM.CreateNode();
            NodeController.AddNode(master);
            m_ID = nodes.Select(y => y.ID).Max();

            FirstRoad = TM.CreateRoad(m_ID, n_ID);
            RoadController.AddRoad(FirstRoad);
            r1_ID = roads.Select(y => y.ID).Max();

            SecondRoad = TM.CreateRoad(m_ID, f_ID);
            RoadController.AddRoad(SecondRoad);
            r2_ID = roads.Select(y => y.ID).Max();

            Dictionary<int, double> d = new Dictionary<int, double>();
            d.Add(r1_ID, FirstRoad.Length);
            d.Add(r2_ID, SecondRoad.Length);
            List<ViewModels.AddNode> list = new List<ViewModels.AddNode>();
            list.Add(nearest);
            list.Add(farthest);
            list.Add(master);
            List<Node> listN = new List<Node>();
            foreach(ViewModels.AddNode n1 in list)
            {
                Node node = new Node
                {
                    ID = 
                }
            }
            
            Node n = nqs.GetNearest(d, list);
        }*/
    }
}