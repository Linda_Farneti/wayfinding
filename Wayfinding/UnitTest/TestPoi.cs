﻿using EntityModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wayfinding.Controllers.WebApi;

namespace Wayfinding.UnitTest
{
    [TestFixture]
    public class TestPoi
    {
        public System.Data.Entity.DbSet<EntityModels.PointOfInterest> pois;
        public List<PointOfInterest> MockPois;
        public int RemovingPoi;
        public int AddingPoi;
        private IDbPointController controller;
        private TestManager TM;
        private bool M;

        public TestPoi()
        {
            TM = TestManager.GetInstance();
            RemovingPoi = Consts.TO_REMOVE;
            AddingPoi = Consts.TO_ADD;
            controller = TM.PointController;
            pois = new WayFindingUniBOEntities1().PointOfInterest;
            MockPois = MockDatabase.GetInstance().PointOfInterest;
            M = TM.Mock;
        }

        private void ClearDatabase()
        {
            if (!M)
            {
                TM.ClearDatabase();
            }
        }

        private int CountPois()
        {
            return M ? MockPois.Count() : pois.Count();
        }

        [TestCase]
        public void AddPoi()
        {
            int TotalPoints = CountPois();
            for (int i = 0; i < AddingPoi; i++)
            {
                controller.AddPoint(TM.CreatePoint());
            }
            Assert.AreEqual(CountPois(), TotalPoints + AddingPoi);
            Assert.AreEqual(M ? MockPois.Where(x => x.Map == Consts.MAP_TEST_ID).ToList().Count() : 
                            pois.Where(x => x.Map == Consts.MAP_TEST_ID).ToList().Count(), AddingPoi);
            ClearDatabase();
        }

        [TestCase]
        public void RemovePoint()
        {
            int TotalPoints = 0;
            int[] added;

            for (int i = 0; i < AddingPoi; i++)
            {
                controller.AddPoint(TM.CreatePoint());
            }

            TotalPoints = CountPois();
            added = M ? MockPois.Where(x => x.Map == Consts.MAP_TEST_ID).Select(y => y.NodeID).ToArray() :
                        pois.Where(x => x.Map == Consts.MAP_TEST_ID).Select(y => y.NodeID).ToArray();
            for (int i = 0; i < RemovingPoi; i++)
            {
                controller.RemovePoint(added[i]);
            }
            Assert.AreEqual(CountPois(), TotalPoints - RemovingPoi);
            ClearDatabase();
        }

        [TestCase]
        public void UpdatePoint()
        {
            int ID = 0;
            ViewModels.AddPoint p = TM.CreatePoint();
            controller.AddPoint(p);
            ID = p.NodeID;
            controller.UpdatePoint(new ViewModels.UpdatePoint {ID = ID, Size = 50 });
            PointOfInterest p1 = M ? MockPois.Where(x => x.NodeID == ID).First() : 
                                     pois.Where(x => x.ID == (pois.Select(y => y.ID).ToList().Max())).First();
            Assert.AreEqual(p1.Size, 50);
            ClearDatabase();
        }
        
    }
}