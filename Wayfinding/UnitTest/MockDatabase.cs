﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wayfinding.ViewModels;

namespace Wayfinding.UnitTest
{
    public class MockDatabase
    {
        public List<Node> Node;
        public List<Road> Road;
        public List<PointOfInterest> PointOfInterest;
        private static MockDatabase Database;

        private MockDatabase()
        {
            Node = new List<Node>();
            Road = new List<Road>();
            PointOfInterest = new List<PointOfInterest>();
        }

        public static MockDatabase GetInstance()
        {
            if(Database == null)
            {
                Database = new MockDatabase();
            }
            return Database;
        }
    }
}