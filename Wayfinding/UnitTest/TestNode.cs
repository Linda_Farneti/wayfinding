﻿using EntityModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wayfinding.Controllers.WebApi;
using Wayfinding.ViewModels;

// ID : 88

namespace Wayfinding.UnitTest
{
    [TestFixture]
    public class TestNode
    {
        public System.Data.Entity.DbSet<EntityModels.Node> nodes;
        public List<Node> MockNodes;
        private int RemovingNode, AddingNode;
        private IDbNodeController controller;
        private TestManager TM;
        private bool M;

        public TestNode()
        {
            TM = TestManager.GetInstance();
            RemovingNode = Consts.TO_REMOVE;
            AddingNode = Consts.TO_ADD;
            controller = TM.NodeController;
            nodes = new WayFindingUniBOEntities1().Node;
            MockNodes = MockDatabase.GetInstance().Node;
            M = TM.Mock;
        }

        private void ClearDatabase()
        {
            if (!M)
            {
                TM.ClearDatabase();
            }
        }

        private int CountNodes()
        {
           return M? MockNodes.Count() : nodes.Count();
        }

        [TestCase]
        public void AddNode()
        {
            int TotalNodes = CountNodes();
            for(int i = 0; i < AddingNode; i++)
            {
                controller.AddNode(TM.CreateNode());
            }
            Assert.AreEqual(CountNodes(), TotalNodes + AddingNode);
           /*if (M)
            {
                Assert.AreEqual(MockNodes.Where(z => z.Map == Consts.MAP_TEST_ID).Count(), AddingNode);
            }
            else
            {
                Assert.AreEqual(nodes.Where(z => z.Map == Consts.MAP_TEST_ID).Count(), AddingNode);
            }*/
            ClearDatabase();
        }

        [TestCase]
        public void RemoveNode()
        {
             int TotalNodes = 0;
             int[] added;
             for (int i = 0; i < AddingNode; i++)
             {
                 controller.AddNode(TM.CreateNode());
             }
             TotalNodes = CountNodes();
             added = M ? MockNodes.Where(x => x.Map == Consts.MAP_TEST_ID).Select(y => y.ID).ToArray():
                         nodes.Where(x => x.Map == Consts.MAP_TEST_ID).Select(y => y.ID).ToArray();
             for(int i = 0; i < RemovingNode; i++)
             {
                 controller.RemoveNode(added[i]);
             }
             Assert.AreEqual(CountNodes(), TotalNodes - RemovingNode);
             ClearDatabase();
        }

        [TestCase]
        public void UpdateNode()
        {
            int ID = 0;
            int lat, lng;
            controller.AddNode(TM.CreateNode());
            ID = M ? MockNodes.Select(x => x.ID).ToList().Max() :
                     nodes.Select(x => x.ID).ToList().Max();
            lat = TM.Randomize(50);
            lng = TM.Randomize(50);
            controller.UpdateNodePosition(new ViewModels.UpdateNode { ID=ID,Latitude = lat, Longitude = lng});
            Node n = M ? MockNodes.Where(x => x.ID == ID).First() :
                         nodes.Where(x => x.ID == ID).First();
            Assert.AreEqual(n.Longitude, lng, 0.05);
            Assert.AreEqual(n.Latitude, lat, 0.05);
            ClearDatabase();
        }
    }
}