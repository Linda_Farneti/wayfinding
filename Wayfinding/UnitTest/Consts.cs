﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.UnitTest
{
    public class Consts
    {
        public static readonly int MAP_TEST_ID = 9;
        public static readonly int TO_ADD = 10;
        public static readonly int TO_REMOVE = 5;
        public static readonly int ROAD_DEFAULT_SIZE = 15;
        public static readonly int ROAD_DEFAULT_LENGTH = 200;
        public static readonly int POINT_DEFAULT_SIZE = 20;
        public static readonly int DEFAULT_NEIGHBOURS = 25;
        public static readonly int NEIGHBOURS_TO_REMOVE = 5;

    }
}