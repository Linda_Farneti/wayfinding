﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class ChangeColor
    {
        public string Element { get; set; }
        public string Color { get; set; }
    }
}