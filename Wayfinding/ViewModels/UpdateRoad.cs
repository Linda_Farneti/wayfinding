﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class UpdateRoad
    {
        public int ID { get; set; }
        public float? NewLength { get; set; }
        public int? NewSize { get; set; }
    }
}