﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class SignupViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}