﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class UpdatePoint
    {
        public int ID { get; set; }
        public string Desc { get;  set; }
        public int? Size { get; set; }
    }
}