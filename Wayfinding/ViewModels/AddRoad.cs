﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class AddRoad
    {
        public float Length { get; set; }
        public int StartNodeID { get; set; }
        public int EndNodeID { get; set; }
        public int Size { get; set; }
        //public Node[] Nodes { get; set; }
        public int Map { get; set; }
    }
}