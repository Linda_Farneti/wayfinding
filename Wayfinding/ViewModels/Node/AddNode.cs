﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class AddNode : IAddNode
    {
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int Map { get; set; }
    }
}