﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wayfinding.ViewModels.Node;

namespace Wayfinding.ViewModels
{
    public class UpdateNode : IUpdateNode
    {
        public int ID { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

    }
}