﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wayfinding.ViewModels.Node
{
    public interface IUpdateNode
    {
        int ID { get; set; }
        float Latitude { get; set; }
        float Longitude { get; set; }
    }
}
