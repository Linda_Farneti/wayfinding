﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels.Node
{
    public class MockUpdateNode : IUpdateNode
    {
        public int ID { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}