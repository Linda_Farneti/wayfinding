﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wayfinding.ViewModels
{
    public interface IAddNode
    {
        float Latitude { get; set; }
        float Longitude { get; set; }
        int Map { get; set; }
    }
}
