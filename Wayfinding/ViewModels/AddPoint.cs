﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.ViewModels
{
    public class AddPoint
    {
        public string Name { get; set; }
        public int NodeID { get; set; }
        public int Size { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public int Map { get; set; }
    }
}