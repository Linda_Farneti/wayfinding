﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.Models
{
    public class MapListDto
    {
        public MapDto[] Maps { get; set; }
    }
}