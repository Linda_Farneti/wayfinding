﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.Models
{
    public class POIsListDto
    {
        public POIDto[] POIs { get; set; }
    }
}