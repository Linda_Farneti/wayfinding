﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.Models
{
    public class RoadDto
    {
        public int Id { get; set; }
        public double Length { get; set; }
        public int StartNode { get; set; }
        public int EndNode { get; set; }
        public int Size { get; set; }
        public int Map { get; set; }
    }
}