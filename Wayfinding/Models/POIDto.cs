﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.Models
{
    public class POIDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NodeID { get; set; }
        public int Size { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public int Map { get; set; }
    }
}