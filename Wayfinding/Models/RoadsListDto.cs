﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.Models
{
    public class RoadsListDto
    {
        public RoadDto[] Roads { get; set; }
    }
}