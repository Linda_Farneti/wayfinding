﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wayfinding.Models
{
    public class IconDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}