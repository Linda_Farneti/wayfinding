import { Node } from "../model/node";
import { State, Item } from "../utility/consts";
import { Pair } from "../utility/pair";

export interface CommandObserver {

    setState(state:State) : void;
    onDrawRoad(size:number): void;
    onMapClicked(e: any): void;
    onShowItem(i:Item, c:boolean) : void;
    onResizeViewIcons(newSize : number) : void;
    onResizeModelIcons(newSize : number) : void;
    onRemoveElements() : void;
    onRemoveIcons(callback:()=>void) : void;
    onAddIcons(s: String) : void;
    onDeleteMap() : void;
   // onExportJson() : string;
   // onImportJson(s: string) : void;
    onSelectPath() : void;
   // onStart(): void;
    onSetDescription(text:string):void;
    onUpdateRoadSize(newSize:number) : void;
    setFinalRoadSize(finalSize:number):void;
    loadMap(Id:number) : void;
}