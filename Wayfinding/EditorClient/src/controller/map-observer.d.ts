import { Circle } from "../view/circle";
import { Line } from "../view/line";

export interface MapObserver{
    onCircleClicked(circle:Circle) : void;
    onLineClicked(line:Line) : void;
    onMovingNode(circle:Circle) : void;
   // setNewNodePosition(circle: Circle): void;
    setFinalNodePosition(circle:Circle) : void;
    findDescription(c:Circle) : void;
}