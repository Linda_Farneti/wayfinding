import { CommandObserver } from "./command-observer";
import { Controller } from "./controller";
import { State, Item } from "../utility/consts";
import { IList } from "../utility/i-list";
import { List } from "../utility/list";

export class BrowserObservable {

    private observers: IList<CommandObserver>;
    private map: L.Map;
    private static instance: BrowserObservable;

    private constructor() {
        this.observers = new List<CommandObserver>();
    }
    public static showDescription(text: string) {
        $("#description").show();
        $("#description_t").val(text);
    }
    public setup() {
        var hide = true;
        var hideEdit = true;
        $("#options").hide();
        $("#viewoptions").click(()=>{
            $("#options").slideToggle();
            if(hide){
                $("#viewoptions > img").attr('src', "Content/WebImages/arrowup.svg");
            }else{
                $("#viewoptions > img").attr('src', "Content/WebImages/arrowdown.svg");
            }
            hide = !hide;
        });

        $("#edit").hide();
        $("#description").hide();
        $("#dtp").click(() => {
            $("#description").fadeToggle();
        });
     //   $("#load_gif").show();
        
        $("#nodeLayer").prop("checked", true);
        $("#iconLayer").prop("checked", true);
        $("#roadLayer").prop("checked", true);

        $("#select").click(() => {
            //$("#load_gif").show();
            this.observers.list().forEach(o => {
                o.setState(State.SELECTION);
            });
            if(hideEdit){
                $("#select > label").text("Close Editor");
                $("#mymap").css("margin-left", "400px");
            }else{
                $("#select > label").text("Open Editor");
                $("#mymap").css("margin-left", "250px");
            }
            hideEdit = !hideEdit;
            $("#edit").fadeToggle("fast");
        });

        $("#setDescription").click(() => {
            this.observers.list().forEach(o => {
                o.onSetDescription(<string>$("#description_t").val());
            });
            $("#description_t").val("");
        });

        $("#drawNode").click(() => {
            this.observers.list().forEach(o => {
                o.setState(State.DRAW_NODE);
            });
        });

        $("#drawRoad").click(() => {
            this.observers.list().forEach(o => {
                o.onDrawRoad(<number>$("#roadSize").val());
            });
        });

        $("#selectPath").click(() => {
            this.observers.list().forEach(o => {
                o.setState(State.SELECT_PATH);
                o.onSelectPath();
            });
        });

        $("#delete").click(()=>{
            this.observers.list().forEach(o => {
                o.onDeleteMap();
            });
        });

        $("#nodeLayer").change(() => {
            var c = $("#nodeLayer").is(':checked');
            this.observers.list().forEach(o => {
                o.onShowItem(Item.NODE, c);
            });
        });

        $("#iconLayer").change(() => {
            var c = $("#iconLayer").is(':checked');
            this.observers.list().forEach(o => {
                o.onShowItem(Item.ICON, c);
            });
        });

        $("#roadSize").mousedown(() => {
            $("#roadSize").mousemove(() => {
                this.observers.list().forEach(o => {
                    o.onUpdateRoadSize(<number>$("#roadSize").val());
                });
            })
        });

        $("#roadSize").mouseup(() => {
            this.observers.list().forEach(o => {
                o.setFinalRoadSize(<number>$("#roadSize").val());
            });
        });

        $("#resize").mousedown(() => {
            $("#resize").mousemove(() => {
                this.observers.list().forEach(o => {
                    o.onResizeViewIcons(<number>$("#resize").val());
                });
            });
        });

        $("#resize").mouseup(() => {
            this.observers.list().forEach(o => {
                o.onResizeModelIcons(<number>$("#resize").val());
            });
        });

        $("#resize").mouseup(() => {
            $("#resize").unbind("mousemove");
        });

        $("#roadLayer").change(() => {
            var c = $("#roadLayer").is(':checked');
            this.observers.list().forEach(o => {
                o.onShowItem(Item.ROAD, c);
            });
        });

        $("#remove").click(() => {
            this.observers.list().forEach(o => {
                o.onRemoveElements();
            });
        });

        $("#removeI").click(() => {
            this.observers.list().forEach(o => {
                o.onRemoveIcons(() => { });
            });
        });

        var a: JQuery<HTMLElement>;
        $(".icons").click(function () {
            a = $(this);
        });

        $(".icons").click(() => {
            this.observers.list().forEach(o => {
                o.onAddIcons(a.attr('name'));
            });
        });

        $("#move").click(() => {
            this.observers.list().forEach(o => {
                o.setState(State.MOVE);
            });
        });

        /*   $("#export").click(() => {
               this.observers.list().forEach(o => {
                      $("#JSONfile").val(o.onExportJson());
               });
           });
   
           $("#import").click(() => {
               this.observers.list().forEach(o => {
                   o.onImportJson(<string>$("#JSONfile").val());
               });
           });*/

           $("#selectPath").click(() => {
            this.observers.list().forEach(o => {
                o.onSelectPath();
            });
        });
       /* $("#start").click(() => {
            this.observers.list().forEach(o => {
                o.onStart();
            });
        });*/
    }

    public addObserver(obs: CommandObserver) {
        this.observers.add(obs);
    }

    public getObservers(): IList<CommandObserver> {
        return this.observers;
    }

    public setMap(map: L.Map) {
        this.map = map;
        (this.map).on("click", (e: any) => {
            BrowserObservable.getInstance().getObservers().list().forEach(obs => {
                obs.onMapClicked(e);
            });
        });
    }
    public static getInstance(): BrowserObservable {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new BrowserObservable();
        }
        return this.instance;
    }
}
