import { IList } from "../utility/i-list";
import { List } from "../utility/list";
import { icon } from "leaflet";
import { Consts } from "../utility/consts";

export class SettingsManager {

    private static instance:SettingsManager;

    public CircleC = {
        color: "grey"
    };

    public RoadC = {
        color: "white"
    }

    public CircleBC = {
        color: "grey"
    }

    public RoadSSC = {
        color: "blue"
    }

    public CircleSSC = {
        color: "blue"
    }

    private SettingsManager(){}

    public setCircleC(color:string){
        this.CircleC = {
            color:color
        }
    }
    public setRoadC(color:string){
        this.RoadC = {
            color:color
        }
    }
    public setCircleBC(color:string){
        this.CircleBC = {
            color:color
        }
    }
    public setRoadSSC(color:string){
        this.RoadSSC = {
            color:color
        }
    }
    public setCircleSSC(color:string){
        this.CircleSSC = {
            color:color
        }
    }

    public static getInstance() : SettingsManager{
        if(this.instance === undefined || this.instance === null){
            this.instance = new SettingsManager();
        }
        return this.instance;
    }
}