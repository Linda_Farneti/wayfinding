import { IList } from "../utility/i-list";
import { List } from "../utility/list";
import { icon } from "leaflet";

export class IconManager {
    private static instance:IconManager;

    public getIconsDB(callback:(icons:IList<string>)=>void) {
        let icons:IList<string> = new List<string>();
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Icon/GetIcons",
            contentType: "application/json", 
            success: (i)=>{
                let a = new Array(i);
                for(let index = 0; index < a[0].length; index++){
                    icons.add(a[0][index].Name);
                }
                $("#load_gif").hide();
                callback(icons);
            },
            dataType: "json"
        });
    }

    public static getInstance() : IconManager{
        if(this.instance === undefined || this.instance === null){
            this.instance = new IconManager();
        }
        return this.instance;
    }
}