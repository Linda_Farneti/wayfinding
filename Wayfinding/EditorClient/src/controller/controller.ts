import { polyline } from 'leaflet';

import { Graph } from '../model/graph';
import { GraphNode } from '../model/graph-node';
import { GraphRoad } from '../model/graph-road';
import { Node } from '../model/node';
import { Point } from '../model/point';
import { Road } from '../model/road';
import { ArrayMap } from '../utility/array-map';
import { Consts, Item, State } from '../utility/consts';
import { DistanceCalculator } from '../utility/distance-calculator';
import { IList } from '../utility/i-list';
import { List } from '../utility/list';
import { Map } from '../utility/map';
import { Pair } from '../utility/pair';
import { PairImpl } from '../utility/pair-impl';
import { Circle } from '../view/circle';
import { Drawer } from '../view/drawer';
import { Line } from '../view/line';
import { MapDrawer } from '../view/map-drawer';
import { BrowserObservable } from './browser-observable';
import { CommandObserver } from './command-observer';
import { MapObserver } from './map-observer';
import { SettingsManager } from './settings-manager';

export class Controller implements CommandObserver, MapObserver {

    private selection: State;
    private nodesClicked: IList<Node>;
    private deselectedNodes: IList<Node>;
    private roadsClicked: IList<Road>;
    private maps:IList<Drawer>;
    private mapCN:Map<Circle,Node>;
    private mapLR:Map<Line,Road>;
    private drawers:IList<Drawer>;
    private graph:Graph;
    private actualStyle:Consts;
    private nodeWindow:Array<Node>;
    private  windowSize:number;
    private sm:SettingsManager;

    public constructor(ds:IList<Drawer>, g:Graph) {
       this.sm = SettingsManager.getInstance();
       this.nodesClicked = new List<GraphNode>();
       this.deselectedNodes = new List<GraphNode>();
       this.roadsClicked = new List<GraphRoad>();
       this.maps = new List<MapDrawer>();
       this.mapCN =  new ArrayMap<Circle, GraphNode>();
       this.mapLR = new ArrayMap<Line, GraphRoad>();
       this.drawers = ds;
       this.graph = g;
       this.actualStyle = this.sm.CircleC;
    }

    private canSelectNode(node : Node) : boolean{
        if ((this.nodesClicked.size() === 0) || (this.nodesClicked.get(this.nodesClicked.list().length-1) !== node)) {
            return true;
        } else {
            if (this.nodesClicked.get(this.nodesClicked.list().length-1) === node){
                this.drawers.list().forEach(d => {
                    d.changeStyle(this.mapCN.getFirst(node),this.sm.CircleBC);
                });
                this.nodesClicked.removeLast(node);
                this.deselectedNodes.add(node);
            }
        }
        return false;
    }

    private updateEdge(r:Road, size:number, poly:Array<Pair<number,number>>){
        let l = this.mapLR.getFirst(r);
        let newLine:Line;
        this.drawers.list().forEach(d => {
            d.removeLine(l);
        });
        for (let i = 0; i < this.mapLR.entries().size(); i++) {
            if (this.mapLR.entries().get(i).getY() === r) {
                this.mapLR.entries().removeIndex(i);
            }
        }
        this.drawers.list().forEach(d => {
            newLine = {color:this.sm.RoadC.color, coordinates:poly, fillColor:this.sm.RoadC.color, opacity:Consts.OPACITY, weight: size};
            d.drawLine(newLine);
            d.changeStyle(newLine, this.sm.RoadSSC);
        })
        this.mapLR.push(newLine, r);
    }

    private removeEdge(r: Road) {
        //console.log("removing edge...");
        //let l = this.mapLR.getFirst(r);
        let l:any = null;
        this.mapLR.entries().list().forEach(x => {
            if(x.getY().getName() === r.getName()){
                l = x.getX();
            }
        });
        this.drawers.list().forEach(d => {
               // console.log("removing line : " + l);
                d.removeLine(l);
        });
        this.graph.removeRoad(r, () => {
          //  console.log("removing road : " + r);
            for (let i = 0; i < this.mapLR.entries().size(); i++) {
                if (this.mapLR.entries().get(i).getY() === r) {
                    this.mapLR.entries().removeIndex(i);
                }
            }
        }); 
    }

    private createRoad(n1: Node, n2: Node, size:number) {
        let polylinePoints: Array<Pair<number, number>>;
        polylinePoints = new Array<Pair<number,number>>(n1.getCoordinates(), n2.getCoordinates());
        let r = this.graph.getRoadBetweenNodes(n1, n2);
        if (r === null || r === undefined) {
            this.addRoad(polylinePoints, n1, n2, size);
     //       console.log("finish function add road controller");
        } else {
           if (r.getSize() !== size) {
                this.removeEdge(r);
                this.addRoad(polylinePoints, n1, n2, size);
           }
        }
       this.nodeInForeground();
    }

    private convertToPair(e:any) : Pair<number,number> {
        let s = e.latlng.toString();
        let ar = (<String>s).substring(7,s.length-1).split(",");
        return new PairImpl<number,number>(parseFloat(ar[0]), parseFloat(ar[1]));
    }

    private resetClickedItems() {
        this.drawers.list().forEach(d => {
            this.nodesClicked.list().forEach(n => {
                d.changeStyle(this.mapCN.getFirst(n), this.sm.CircleBC);
            });
            this.roadsClicked.list().forEach(r => {
                d.changeStyle(this.mapLR.getFirst(r), this.sm.RoadC);
            });
            d.setDraggableMap(true);
        });
        this.nodesClicked.clear();
        this.roadsClicked.clear();
        this.selection = State.SELECTION;
    }

    private removeNode(n: Node) {
        let c = this.mapCN.getFirst(n);
        this.mapCN.entries().remove(new PairImpl<Circle, Node>(c, n));
        this.graph.removeNode(n);
        this.drawers.list().forEach(d => {
            d.removeCircle(c);
        });
    }

    private createNode(coord: Pair<number, number>) {
        if (this.graph.positionAvailableForNodes(coord)) {
            this.graph.addNode(coord, (n)=>{
                let c:Circle;
                this.drawers.list().forEach(d => {
                    c = {color: this.sm.CircleBC.color, coordinates:coord, fillColor:this.sm.CircleC.color, opacity:Consts.OPACITY, ray:Consts.CIRCLE_RAY};
                    c = d.drawCircle(c);
                });
                this.mapCN.push(c, n);
            });
        }
    }
  
    private addRoad(poly: Array<Pair<number,number>>, n1: Node, n2: Node, size: number) {
        this.graph.addRoad(n1, n2, size, (r)=>{
            let l:Line;
            this.drawers.list().forEach(d => {
                l = {color: this.sm.RoadC.color, coordinates:poly, fillColor: this.sm.RoadC.color, opacity:Consts.OPACITY, weight: size};
                d.drawLine(l);
            })
            this.mapLR.push(l, r);
        });
    }

    private nodeInForeground(){
        this.drawers.list().forEach(d => {
            d.showItem(Item.NODE, false);
            d.showItem(Item.NODE, true);
        });
    }

    public loadMap(id:number){
        let i = id; // useless
        this.graph.loadNodes((nodes:IList<Node>)=>{
            nodes.list().forEach(n => {
                let c:Circle;
                this.drawers.list().forEach(d => {
                    c = {color:this.sm.CircleBC.color, coordinates:n.getCoordinates(), fillColor:this.sm.CircleC.color, opacity:Consts.OPACITY, ray:Consts.CIRCLE_RAY};
                    c = d.drawCircle(c);
                });
                this.mapCN.push(c, n);
            });
            this.graph.loadRoads((roads:IList<Road>)=>{
                roads.list().forEach(r => {
                    let l:Line;
                    this.drawers.list().forEach(d => {
                        l = {color:this.sm.RoadC.color, coordinates:[r.getCoupleNodes().getX().getCoordinates(),r.getCoupleNodes().getY().getCoordinates() ], fillColor:"white", opacity:Consts.OPACITY, weight: r.getSize()};
                        d.drawLine(l);
                    })
                    this.mapLR.push(l, r);
                   
                })  
            });
            this.graph.loadPois((points:IList<Point>)=>{
                points.list().forEach(p => {
                    this.drawers.list().forEach(d=> {
                        let c:Circle = this.mapCN.getFirst(this.graph.getNodeById(p.nodeID));
                        d.addIconToCircle(c, p.path, p.size);
                    });
                });
            });
        });
    }

    public setState(newState: State){
        this.selection = newState;
        if (this.selection === State.MOVE) {
            this.drawers.list().forEach(d => {
                d.setDraggableMap(false);
            });
        } else {
            this.drawers.list().forEach(d => {
                d.setDraggableMap(true);
            });
        }
        if (this.selection === State.DRAW_NODE) {
            this.actualStyle = this.sm.CircleSSC;
        }
        this.drawers.list().forEach(d => {
            d.resetPath();
        });
    }

    /* STRADE RIPETUTE SE DISEGNATE ALLO STESSO MOMENTO                                                 PROBLEM */                                         
    public onDrawRoad(size:number): void {
        let n1, n2: Node;
        if (this.nodesClicked.list().length >= 2) {
            for (let i = 0; i < this.nodesClicked.list().length-1; i++) {
                n1 = this.nodesClicked.get(i);
                n2 = this.nodesClicked.get(i+1);

                this.createRoad(n1, n2, size);
            }
        }
        this.resetClickedItems();
    }

    public onDeleteMap(){
        this.graph.deleteMap();
    }
    
    public onSetDescription(text:string){
        console.log(text);
        this.nodesClicked.list().forEach(n => {
            this.graph.onDescriptionSet(n.getId() , text);
        });
        this.resetClickedItems();
    }

    public onUpdateRoadSize(newSize:number){
       this.roadsClicked.list().forEach(r => {
           let polyline = new Array<Pair<number,number>>(r.getCoupleNodes().getX().getCoordinates(), r.getCoupleNodes().getY().getCoordinates());
           this.updateEdge(r, newSize, polyline);
       });
    }

    public onShowItem(i: Item, c: boolean) {
        this.drawers.list().forEach(d => {
            d.showItem(i,c);
        });
        if (i === Item.ROAD && c) {
            this.nodeInForeground();
        }
    }
    
    private removeConnectedItems(callback:()=>void){
        let roads:IList<Road> = new List<Road>();
        this.roadsClicked.list().forEach(r => { this.removeEdge(r); });
        this.nodesClicked.list().forEach(n => {
            if ((this.mapCN.getFirst(n) != null) && this.nodesClicked.contains(n)) {
                roads.addAll(this.graph.getRoadsFromNode(n).list());
                roads.list().forEach(r => {
                    if(!this.roadsClicked.contains(r)){
                        this.removeEdge(r);
                    }
                });
                this.drawers.list().forEach(d => { d.removeIcon(this.mapCN.getFirst(n)); });
                this.graph.removePoint(n.getId());
                roads.clear();
            }
        });
        callback();
    }

    public onRemoveElements() : void {
        this.removeConnectedItems(()=>{
            this.nodesClicked.list().forEach(n => {
                if ((this.mapCN.getFirst(n) != null) && this.nodesClicked.contains(n)) {
                    this.removeNode(n);
                }
            });
        });
        this.roadsClicked.clear();
        this.nodesClicked.clear();
    }

    public onRemoveIcons(callback:()=> void) : void {
        this.nodesClicked.list().forEach(n => {
            if (this.nodesClicked.contains(n) && !this.deselectedNodes.contains(n)) {
                this.drawers.list().forEach(d => {
                    d.removeIcon(this.mapCN.getFirst(n));
                });
                this.graph.removePoint(n.getId());
            }
        });
        callback();
        this.resetClickedItems();
        this.deselectedNodes.clear();
    }

    public onAddIcons(s: string) : void {
        let savedClickedNodes = this.nodesClicked.list();
        this.onRemoveIcons(() =>{
            savedClickedNodes.forEach (n => {
                  if (!this.deselectedNodes.contains(n)) {
                     this.graph.addIconToPoint(s, n.getId(), Consts.DEFAULT_ICON_SIZE, "", s, ()=>{
                          let c = this.mapCN.getFirst(n);
                          this.drawers.list().forEach(d => {
                              d.addIconToCircle(c, s, Consts.DEFAULT_ICON_SIZE);
                          });
                     });
                  }
              });
              this.deselectedNodes.clear();
        });
    }

    public onResizeViewIcons(newSize: number) : void{
        this.drawers.list().forEach(d => {
            this.nodesClicked.list().forEach(nc => {
                d.resizeCircleIcon(this.mapCN.getFirst(nc), newSize);
            });
        });
    }

    public onResizeModelIcons(newSize: number) : void{
        this.nodesClicked.list().forEach(nc => {
            this.graph.resizePointIcon(nc.getId(), newSize);
        });
    }

    public onMapClicked(e:any) : void {
        let pc = this.convertToPair(e);      
        if (this.selection === State.DRAW_NODE) {
            this.createNode(pc);
        }
    }

    public onExportJson() : string {
       return this.graph.setJSON();
    }

    public onCircleClicked(circle:Circle) : void {
        let node = this.mapCN.getSecond(circle);
        if (this.canSelectNode(node)) {
            this.nodesClicked.add(node);
            this.drawers.list().forEach(d => {
                d.changeStyle(circle, this.sm.CircleSSC);
            });
        }
    }

    public onLineClicked(line:Line) : void {
        let road = this.mapLR.getSecond(line);
        if (this.roadsClicked.list().indexOf(road) < 0) {
            this.roadsClicked.add(road);
            this.drawers.list().forEach(d => {
                d.changeStyle(line, this.sm.RoadSSC);
            });
        } else {
            this.drawers.list().forEach(d => {
                d.changeStyle(line, this.sm.RoadC);
            });
            this.roadsClicked.removeIndex(this.roadsClicked.list().indexOf(road));
        }
    }

    public onSelectPath() {
        let path: IList<Road>;
        let drawPath: IList<Line> = new List<Line>();
        this.mapLR.keys().list().forEach(l => {
            this.drawers.list().forEach(d => {
                d.changeStyle(l, this.sm.RoadC);
            });
        });
        if (this.nodesClicked.size() >= 2) {
            this.graph.getPath(this.nodesClicked.get(0), this.nodesClicked.get(1), (list)=>{
                list.list().forEach(x => {
                    this.mapLR.entries().list().forEach(lr => {
                        if(lr.getY().getName() == x){
                            let l = lr.getX();
                            drawPath.add(l);
                        }
                    });
                });
                this.drawers.list().forEach(d => {
                    d.selectPath(drawPath);
                });
            });
        }
        this.resetClickedItems();
    }

    public onMovingNode(circle: Circle){
        if(this.selection === State.MOVE){
            let movingNode = this.mapCN.getSecond(circle);
            movingNode.setCoordinates(circle.coordinates);
            this.graph.getRoadsFromNode(movingNode).list().forEach(r => {
                let l = this.mapLR.getFirst(r);
                let oldSize:number;
                this.mapLR.entries().list().forEach(x => {
                    if(x.getY().getName() === r.getName()){
                        l = x.getX();
                    }
                });
                this.drawers.list().forEach(d => {
                    oldSize = l.weight;
                    d.removeLine(l);
                });
                for (let i = 0; i < this.mapLR.entries().size(); i++) {
                    if (this.mapLR.entries().get(i).getY() === r) {
                        this.mapLR.entries().removeIndex(i);
                    }
                }
                let otherNode = r.getCoupleNodes().getX() !== movingNode ? r.getCoupleNodes().getX() : r.getCoupleNodes().getY();
                let polylinePoints = new Array<Pair<number,number>>(movingNode.getCoordinates(), otherNode.getCoordinates());
                this.drawers.list().forEach(d => {
                    l = {color:this.sm.RoadC.color, coordinates:polylinePoints, fillColor:this.sm.RoadC.color, opacity:Consts.OPACITY, weight:oldSize};
                    d.drawLine(l);      
                })
                this.mapLR.push(l, r);      
            });
        }
    }

    public setFinalNodePosition(circle:Circle){
        let node = this.mapCN.getSecond(circle);
        node.setCoordinates(circle.coordinates);
        this.graph.updateNodePosition(node.getId(), circle.coordinates.getX(), circle.coordinates.getY());
        this.graph.getRoadsFromNode(node).list().forEach((r)=>{
            let otherNode = r.getCoupleNodes().getX() !== node ? r.getCoupleNodes().getX() : r.getCoupleNodes().getY();
            let newLength = DistanceCalculator.getDistanceNodeToNode(node, otherNode);
            this.graph.updateRoad(r, newLength, undefined);
        });
    }

    public setFinalRoadSize(finalSize:number){
        this.roadsClicked.list().forEach((r)=>{
            this.graph.updateRoad(r, undefined, finalSize);
            this.drawers.list().forEach((d) => {
                d.changeStyle(this.mapLR.getFirst(r), this.sm.RoadC);
            });
        });
        this.roadsClicked.clear();
    }

    public findDescription(c:Circle){
        let n = this.mapCN.getSecond(c);
        BrowserObservable.showDescription(this.graph.getPointDescription(n.getId()));
    }
}