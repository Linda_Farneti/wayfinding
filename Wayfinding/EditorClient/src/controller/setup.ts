import * as L from 'leaflet';

import { Graph } from '../model/graph';
import { GraphCreator } from '../model/graph-creator';
import { Consts } from '../utility/consts';
import { IList } from '../utility/i-list';
import { List } from '../utility/list';
import { Drawer } from '../view/drawer';
import { MapDrawer } from '../view/map-drawer';
import { BrowserObservable } from './browser-observable';
import { Controller } from './controller';
import { IconManager } from './icon-manager';
import { SettingsManager } from './settings-manager';

declare var MapId: number;
declare var ImagePath: string;

class Setup {

    private controller: Controller;
    private graph: Graph;
    private drawers: IList<Drawer>;
    private iconManager: IconManager;

    public setup(): void {
        this.getMapInfo((longitude, latitude, zoom, name) => {
            this.getIcons(() => {
                this.initComponents(longitude, latitude, zoom); 
            })
            $("#mapname > h1").text(name);
        });
    }

    private getMapInfo(callback: (Longitude: number, Latitude: number, Zoom: number, Name:string) => void) {
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Map/GetMapFromID?ID=" + MapId,
            contentType: "application/json",
            data: null,
            success: (data) => {
                callback(data.Longitude, data.Latitude, data.Zoom, data.Name); 
            },
            dataType: "json"
        })
    }

    private getIcons(callback: () => void) {
        IconManager.getInstance().getIconsDB((icons) => {
            icons.list().forEach(i => {
                var r = $('<input/>').attr({
                    class: "icons",
                    type: "image",
                    src: ImagePath + "/" + i + Consts.ICON_EXT,
                    name: i,
                    width: "40",
                    height: "40",
                    title: i,
                });
                $("#iconsList").append(r);
            }); 
            callback();   
        });
    }

    private initComponents(Longitude: number, Latitude: number, Zoom: number) {
        let b = BrowserObservable.getInstance();
        let sm = SettingsManager.getInstance();
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Settings/GetColors",
            contentType: "application/json",
            data: null,
            success: (i) => {
                sm.setCircleC(i[0]);
                sm.setCircleBC(i[1]);
                sm.setRoadC(i[2]);
                sm.setRoadSSC(i[3]);
                sm.setCircleSSC(i[4]);
               },
            dataType: "json"
        })
        let mymap = L.map('mymap').setView([Latitude, Longitude], Zoom);
        this.graph = new GraphCreator(MapId);
        this.drawers = new List<MapDrawer>(new Array<MapDrawer>(new MapDrawer(mymap, ImagePath)));
        this.controller = new Controller(this.drawers, this.graph);
        this.drawers.list().forEach(d => {
            d.addObserver(this.controller);
        });
        b.addObserver(this.controller);
        b.setMap(mymap);
        b.setup();
        this.controller.loadMap(MapId);
    }
}

$(document).ready(() => {
    new Setup().setup();
});

