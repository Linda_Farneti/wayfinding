import { Pair } from "../utility/pair";
import { Road } from "./road";
import { Node } from "./node";
import { IList } from "../utility/i-list";
import { Point } from "./point";

export interface Graph {
    addNode(coord : Pair<number,number>, callback:(n:Node)=>void): void;
    positionAvailableForNodes(n: Pair<number, number>): boolean;
    getSelectedNode(): Node;
    getSelectedRoad(): Road;
    addRoad(n1: Node, n2: Node, size:number, callback:(r:Road)=>void): void;
    getRoads(): IList<Road>;
    roadExist(n1:Node, n2:Node) : boolean;
    getRoadsFromNode(node:Node) : IList<Road>;
    getNodeById(name: number): Node;
    removeRoad(r:Road, callback:()=>void): void;
    removeNode(n: Node): void;
    removePoint(n: number): void;
    deleteMap() : void;
    setJSON() : string;
    getPath(startNode: Node, endNode:Node,callback:(r:IList<number>)=>void) : void;
    addIconToPoint(name:string, nodeID:number, size:number, description:string, path:string, callback:()=>void): void;
    resizePointIcon(id: number, size: number): void;
    getRoadBetweenNodes(n1: Node, n2: Node): Road;
    updateNodePosition(nodeId: number, latitude:number, longitude:number) : void;
    updateRoad(road:Road, newLength?:number, newSize?:number) : void;
    onDescriptionSet(nodeID:number, text:string) : void;
    getPointDescription(nodeId:number) : string;
    loadNodes(callback:(nodes:IList<Node>)=>void) : void;
    loadRoads(callback:(roads:IList<Road>)=>void) : void;
    loadPois(callback:(points:IList<Point>)=>void) : void;
}