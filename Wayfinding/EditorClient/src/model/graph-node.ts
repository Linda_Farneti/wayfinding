import { Node } from "./node";
import { Pair } from "../utility/pair";

export class GraphNode implements Node {

    private id: number;
    private coord: Pair<number,number>;

    public constructor(id:number, coord:Pair<number,number>){
        this.id = id;
        this.coord = coord;
    }
    
    public getId(): number {
        return this.id;
    }

    public setId(id:number) : void{
        this.id = id;
    }

    public getCoordinates(): Pair<number,number> {
        return this.coord;
    }

    public setCoordinates(p: Pair<number,number>): void {
        this.coord = p;
    }

    public getLatitude() : number{          
        return this.getCoordinates().getX();
    }

    public getLongitude() : number{
        return this.getCoordinates().getY();
    }
    
    public toString(): string{
        return "ID = ["+ this.id + "]" + "Coord : [" + this.coord.getX() +"," + this.coord.getY()+"]";
    }
}