export interface Point{
    id:number;
    name:string;
    nodeID:number;
    size:number;
    description:string;
    path:string;
}