import { Pair } from "../utility/pair";

export interface Node {
    getId(): number;
    setId(id:number) : void;
    getCoordinates(): Pair<number,number>;
    setCoordinates(p: Pair<number,number>): void;
    getLatitude(): number;          // GET THE Y ORDINATE
    getLongitude(): number;         // GET THE X ABSCISSA
    toString(): string;
}