import {Node} from "./node";
import { Pair } from "../utility/pair";

export interface Road {
    getName(): number;
    getCoupleNodes(): Pair<Node, Node>;
    getDistance(): number;
    getSize() : number;
    equals(n1:Node, n2:Node): boolean;
    toString(): String;
}