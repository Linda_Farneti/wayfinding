import { ArrayMap } from '../utility/array-map';
import { DistanceCalculator } from '../utility/distance-calculator';
import { IList } from '../utility/i-list';
import { List } from '../utility/list';
import { Map } from '../utility/map';
import { Pair } from '../utility/pair';
import { PairImpl } from '../utility/pair-impl';
import { Graph } from './graph';
import { GraphNode } from './graph-node';
import { GraphRoad } from './graph-road';
import { Node } from './node';
import { Point } from './point';
import { Road } from './road';

export class GraphCreator implements Graph {
    private nodes: IList<Node>;
    private roads: IList<Road>;
    private points: IList<Point>;
    private nodeSelect: Node;
    private roadSelect: Road;
    private readonly mapId: number;

    public constructor(mapId: number) {
        this.nodes = new List<GraphNode>();
        this.roads = new List<GraphRoad>();
        this.points = new List<Point>();
        this.nodeSelect = null;
        this.mapId = mapId;
    }

    private getNearest(map: Map<Node, Number>, t: IList<Node>): Node {
        let n: Node;
        let distance = Number.POSITIVE_INFINITY;
        t.list().forEach(p => {
            if (map.getSecond(p) < distance) {
                n = p;
                distance = <number>map.getSecond(p);
            }
        });
        return n;
    }

    private getNeighbor(node: Node) {
        let neighbors: IList<Node> = new List<Node>();
        this.getRoadsFromNode(node).list().forEach(r => {
            neighbors.add(r.getCoupleNodes().getX() === node ? r.getCoupleNodes().getY() : r.getCoupleNodes().getX());
        });
        return neighbors;
    }

    private compactForJSON(): void {
        for (let i = 0; i < this.nodes.size(); i++) {
            if (this.nodes.get(i).getId() !== i) {
                this.points.list().forEach(p => {
                    if (p.id === this.nodes.get(i).getId()) {
                        p.id = i;
                    }
                });
                this.nodes.get(i).setId(i);
            }
        }
    }

    private getPointByNodeId(id: number) {
        let point: Point = undefined;
        this.points.list().forEach(p => {
            if (p.nodeID === id) {
                point = p;
            }
        });
        return point;
    }

    public deleteMap(){
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Map/DeleteMap?ID="+this.mapId,
            contentType: "application/json",
           // data: JSON.stringify({ ID: this.mapId }),
            success: () => {
                window.location.href = "http://localhost/Wayfinding/GetStarted";
            },
            dataType: "json"
        });
    }

    public getRoadBetweenNodes(n1: Node, n2: Node): Road {
        let road: Road;
        this.getRoadsFromNode(n1).list().forEach(r => {
            if (r.getCoupleNodes().getX() === n2 || r.getCoupleNodes().getY() === n2) {
                road = r;
            }
        });
        return road;
    }

    public roadExist(n1: Node, n2: Node): boolean {
        let r = this.getRoadBetweenNodes(n1, n2);
        if (r != null && r != undefined) {
            return true;
        } else {
            return false;
        }
    }

    public addNode(coord: Pair<number, number>, callback: (n: Node) => void) {
        let n: Node;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Node/AddNode",
            contentType: "application/json",
            data: JSON.stringify({ Latitude: coord.getX(), Longitude: coord.getY(), Map: this.mapId }),
            success: (i) => {
                n = new GraphNode(Number(i.ID), coord);
                this.nodes.add(n);
                callback(n);
            },
            dataType: "json"
        });
    }

    public addIconToPoint(name: string, nodeID: number, size: number, description: string, path: string, callback: () => void) {
        let newPoint: Point;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Point/AddPoint",
            contentType: "application/json",
            data: JSON.stringify({ Name: name, NodeID: nodeID, Size: size, Description: description, Icon: path, Map: this.mapId }),
            complete: (i) => {
                let id = Number(i.responseText);
                newPoint = { id, name, nodeID, size, description, path };
                this.points.add(newPoint);
                callback();
            },
            dataType: "json"
        });
    }

    public positionAvailableForNodes(coord: Pair<number, number>): boolean {
        let newNode = new GraphNode(this.nodes.size() + 1, coord)
        for (let i = 0; i < this.nodes.list().length; i++) {
            if (DistanceCalculator.getDistanceNodeToNode(this.nodes.get(i), newNode) < 20) {
                return false;
            }
        }
        return true;
    }

    public getSelectedNode(): Node {
        return this.nodeSelect;
    }

    public getSelectedRoad(): Road {
        return this.roadSelect;
    }

    public addRoad(n1: Node, n2: Node, size: number, callback: (r: Road) => void) {
        let r: Road;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Road/AddRoad",
            contentType: "application/json",
            data: JSON.stringify({ Length: DistanceCalculator.getDistanceNodeToNode(n1, n2), StartNodeID: n1.getId(), EndNodeID: n2.getId(), Size: size, Map: this.mapId }),
            complete: (i) => {
                r = new GraphRoad(Number(i.responseText), n1, n2, size, this.mapId);
                // console.log("Road adde in model");
                this.roads.add(r);
                callback(r);
            },
            dataType: "json"
        });
    }

    public getRoads(): IList<Road> {
        return this.roads;
    }

    public getRoadsFromNode(node: Node): IList<Road> {
        let roadVet: IList<Road> = new List<GraphRoad>();
        this.roads.list().forEach(r => {
            if (r.getCoupleNodes().getX() === node || r.getCoupleNodes().getY() === node) {
                roadVet.add(r);
            }
        });
        return roadVet;
    }

    public removeRoad(r: Road, callback: () => void) {
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Road/RemoveRoad?ID="+r.getName(),
            contentType: "application/json",
          //  data: JSON.stringify({ ID: r.getName() }),
            complete: () => {
                this.roads.remove(r);
                callback();
            },
            dataType: "json"
        });

    }

    public removeNode(n: Node) {
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Node/RemoveNode?ID="+n.getId(),
            contentType: "application/json",
         //   data: JSON.stringify({ ID: n.getId() }),
            complete: () => {
                this.nodes.removeIndex(this.nodes.indexOf(n));
            },
            dataType: "json"
        });
    }


    public removePoint(n: number) {
        let num;
        let pointToRemove = this.getPointByNodeId(n);
        if (pointToRemove !== undefined) {
            $.ajax({
                type: "POST",
                url: "http://localhost/Wayfinding/Api/Point/RemovePoint?ID="+n,
                contentType: "application/json",
               // data: JSON.stringify({ ID: n }),
                complete: () => {
                    this.points.remove(pointToRemove);
                },
                dataType: "json"
            });
        }
    }

    public getPointDescription(nodeId: number) {
        let desc: string = "";
        this.points.list().forEach(p => {
            if (p.nodeID == nodeId) {
                desc = p.description;
            }
        });
        return desc;
    }
    public getNodeById(id: number): Node {
        let node: Node;
        this.nodes.list().forEach(n => {
            if (n.getId() === id) {
                node = n;
            }
        });
        return node;
    }

    public setJSON(): string {
        this.compactForJSON();
        return JSON.stringify(new Array<any>(this.nodes, this.roads, this.points));
    }

    public onDescriptionSet(nodeID: number, text: string) {
        let point = this.points.list().forEach(p => {
            if (p.nodeID == nodeID) {
                $.ajax({
                    type: "POST",
                    url: "http://localhost/Wayfinding/Api/Point/UpdatePoint",
                    contentType: "application/json",
                    data: JSON.stringify({ ID: nodeID, Size:null, Desc: text }),
                    complete: () => {
                        p.description = text;
                    },
                    dataType: "json"
                });
            }
        });
    };

    public getPath(startNode: Node, endNode: Node, callback: (r:IList<number>) => void): void {
        var a = startNode;
        var b = endNode;
        let roadVet: IList<number> = new List<number>();
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Road/GetPath?FirstNode="+startNode.getId()+"&SecondNode="+endNode.getId()+"&MapID="+ this.mapId,
            contentType: "application/json",
            //data: JSON.stringify({ FirstNode: startNode, SecondNode:endNode, MapID: this.mapId }),
            success: (i) => {
                let x = new Array(i);
                for(let a = 0; a < x[0].length; a++){
                    roadVet.add(x[0][a].Id);
                }
                callback(roadVet);
            },
            dataType: "json"
        });
        return null;
    }

    /*public getPath(startNode: Node, endNode: Node): IList<Road> {

        let s: IList<Node> = new List<Node>();
        let t: IList<Node> = new List<Node>();
        let f: Map<Node, number> = new ArrayMap<Node, number>();
        let j: Map<Node, Node> = new ArrayMap<Node, Node>();

        let neighbors: IList<Node> = new List<Node>();
        let distance: number;
        let nearest: Node;

        this.nodes.list().forEach(node => {
            f.push(node, Number.POSITIVE_INFINITY);
            j.push(node, undefined);
        });

        f.replace(startNode, startNode, 0);
        this.getNeighbor(startNode).list().forEach(n => {
            f.replace(n, n, this.getRoadBetweenNodes(startNode, n).getDistance());
        });
        t.addAll(this.nodes.list());
        t.remove(startNode);

        while (!t.isEmpty()) {
            nearest = this.getNearest(f, t);
            if (this.getNeighbor(startNode).contains(nearest)) {
                j.replace(nearest, nearest, startNode);
            }
            t.remove(nearest);
            s.add(nearest);
            neighbors = this.getNeighbor(nearest);
            neighbors.removeAll(s.list());
            neighbors.list().forEach(n => {
                distance = f.getSecond(nearest) + this.getRoadBetweenNodes(nearest, n).getDistance();
                if (distance < f.getSecond(n)) {
                    f.replace(n, n, distance);
                    j.replace(n, n, nearest);
                }
            });
        }
        let path: IList<Road> = new List<Road>();
        let next = endNode;
        let prev;
        do {
            prev = j.getSecond(next);
            path.add(this.getRoadBetweenNodes(next, prev));
            next = prev;
        } while (next != startNode);

        return path;
    }*/

    public resizePointIcon(id: number, size: number) {
        let point: Point = this.getPointByNodeId(id);
        let old: number;
        if (point !== undefined) {
            $.ajax({
                type: "POST",
                url: "http://localhost/Wayfinding/Api/Point/UpdatePoint",
                contentType: "application/json",
                data: JSON.stringify({ ID: id, Size: size, Desc:null }),
                complete: () => {
                    old = point.size;
                    point.size = size;
                    //                 console.log("Resize icon of node[ "+id+" ] from size "+old+" to size "+ size);
                },
                dataType: "json"
            });
        }
    }

    public updateNodePosition(nodeId: number, latitude: number, longitude: number) {
        let node = this.getNodeById(nodeId);
       
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Node/UpdateNodePosition",
            contentType: "application/json",
            data: JSON.stringify({ ID: nodeId, Latitude: parseFloat(latitude.toString()), Longitude: parseFloat(longitude.toString()) }),
            dataType: "json"
        });
    }

    public updateRoad(r: Road, newLength?: number, newSize?: number) {
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Road/UpdateRoad",
            contentType: "application/json",
            data: JSON.stringify({ ID: r.getName(), NewLength: newLength, NewSize: newSize }),
            dataType: "json"
        });
    }

    public loadNodes(callback: (nodes: IList<Node>) => void): void {
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Node/GetNodes?MapID=" + this.mapId,
            contentType: "application/json",
            success: (d) => {
                let a = new Array(d);
                for (let i = 0; i < a[0].length; i++) {
                    this.nodes.add(new GraphNode(a[0][i].Id, new PairImpl<number, number>(a[0][i].Latitude, a[0][i].Longitude)));
                }
                callback(this.nodes);
            },
            dataType: "json"
        });
    }

    public loadRoads(callback: (c_roads: IList<Road>) => void): void {
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Road/GetRoads?MapID=" + this.mapId,
            contentType: "application/json",
            success: (d) => {
                let a = new Array(d);
                let c_roads: IList<Road> = new List<Road>();
                for (let i = 0; i < a[0].length; i++) {
                    let StartNode: Node, EndNode: Node;
                    this.nodes.list().forEach(n => {
                        if (n.getId() == a[0][i].StartNode) {
                            StartNode = n;
                        } else if (n.getId() == a[0][i].EndNode) {
                            EndNode = n;
                        }
                    });
                    c_roads.add(new GraphRoad(a[0][i].Id, StartNode, EndNode, a[0][i].Size, a[0][i].Map));
                    this.roads.add(new GraphRoad(a[0][i].Id, StartNode, EndNode, a[0][i].Size, a[0][i].Map));
                }
                callback(c_roads);
            },
            dataType: "json"
        });
    }

    public loadPois(callback: (points: IList<Point>) => void): void {
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Point/GetPoints?MapID=" + this.mapId,
            contentType: "application/json",
            success: (d) => {
                let a = new Array(d);
                let newPoint: Point, id, name, nodeID, size, description, path;
                for (let i = 0; i < a[0].length; i++) {
                    id = a[0][i].Id;
                    name = a[0][i].Name;
                    nodeID = a[0][i].NodeID;
                    size = a[0][i].Size;
                    description = a[0][i].Description;
                    path = a[0][i].Icon,
                        newPoint = { id, name, nodeID, size, description, path };
                    this.points.add(newPoint);
                }
                callback(this.points);
            },
            dataType: "json"
        });
    }
}
