import { DistanceCalculator } from '../utility/distance-calculator';
import { Pair } from '../utility/pair';
import { PairImpl } from '../utility/pair-impl';
import { Node } from './node';
import { Road } from './road';


export class GraphRoad implements Road {

    private name: number;
    private startNode: Node;
    private endNode: Node;
    private distance : number; 
    private size:number;
    private map:number;

    public constructor(n: number, n1:Node, n2:Node, size:number, map:number) {
        this.name = n;
        this.startNode = n1;
        this.endNode = n2;
        this.size = size;
        this.distance = DistanceCalculator.getDistanceNodeToNode(this.startNode, this.endNode);
        this.map = map;
    }


    public getName() : number{
        return this.name;
    }

    public getCoupleNodes() : Pair<Node, Node>{
        let c = new PairImpl<Node, Node>(this.startNode, this.endNode);
        return c;
    }

    public getDistance() : number{
        return this.distance;
    }

    public getSize() : number{
        return this.size;
    }

    public equals(n1:Node, n2:Node){
        if((this.startNode === n1 || this.startNode === n2) && (this.endNode === n1 || this.endNode === n2)){
            return true;
         }
         return false;
    }
    
    public toString() : string{
        return "    ID[" + this.name + "]  StartNode[" + this.startNode.getId() + "]   EndNode [" + this.endNode.getId()+ "]  Distance: " + this.distance;
    }
}