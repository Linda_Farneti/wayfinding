export interface Element {
    color:string;
    fillColor:string;
    opacity: number;
}