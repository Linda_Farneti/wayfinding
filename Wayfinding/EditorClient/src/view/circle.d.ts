import { Pair } from "../utility/pair";
import { Element } from "./element";

export interface Circle extends Element {
    ray:number;
    coordinates:Pair<number, number>;
}