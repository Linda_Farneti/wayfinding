import * as L from 'leaflet';
import { MapObserver } from '../controller/map-observer';
import { ArrayMap } from '../utility/array-map';
import { Consts, Item } from '../utility/consts';
import { IList } from '../utility/i-list';
import { List } from '../utility/list';
import { Map } from '../utility/map';
import { PairImpl } from '../utility/pair-impl';
import { Circle } from './circle';
import { Drawer } from './drawer';
import { Line } from './line';
import { SettingsManager } from '../controller/settings-manager';

export class MapDrawer implements Drawer {
    private map: any;
    private mapObserver: IList<MapObserver>;
    private mapCLC: Map<Circle, L.Circle>;
    private mapLPL: Map<Line, L.Polyline>;
    private mapCM: Map<Circle, L.Marker>;
    private pathList: L.Polyline;
    private draggableMap: boolean = true;
    private markerL = L.layerGroup();
    private nodesL = L.layerGroup();
    private roadL = L.layerGroup();
    private pathL = L.layerGroup();
    private manMarker: L.Marker;
    private readonly imagePath: string;

    public constructor(map: any, imagePath: string) {
        this.map = map;
        this.mapObserver = new List<MapObserver>();
        this.mapCLC = new ArrayMap<Circle, L.Circle>();
        this.mapLPL = new ArrayMap<Line, L.Polyline>();
        this.mapCM = new ArrayMap<Circle, L.Marker>();
        this.drawMap();
        this.imagePath = imagePath;
    }

    public onPositionUpdate(x: number, y: number) {
        if (this.manMarker != undefined && this.manMarker != null) {
            this.map.removeLayer(this.manMarker);
        }
        let iconMan = L.icon({
            iconUrl: this.imagePath + "/marker" + Consts.ICON_EXT,
            iconSize: [40, 40]
        });
        this.manMarker = L.marker([x, y]);
        this.manMarker.setIcon(iconMan);    
        this.manMarker.addTo(this.map);
    }

    private onCircleDropped(newCircle: L.Circle, event: any): void {
        if (!this.draggableMap) {
            this.map.dragging.disable();
            let { lat: circleStartingLat, lng: circleStartingLng } = newCircle.getLatLng();
            let { lat: mouseStartingLat, lng: mouseStartingLng } = event.latlng;
            this.map.on('mousemove', (event: any) => {
                setTimeout(() => {
                    let { lat: mouseNewLat, lng: mouseNewLng } = event.latlng;
                    let latDifference = mouseStartingLat - mouseNewLat;
                    let lngDifference = mouseStartingLng - mouseNewLng;
                    newCircle.setLatLng([circleStartingLat - latDifference, circleStartingLng - lngDifference]);
                    let circle = this.mapCLC.getFirst(newCircle);
                    circle.coordinates = new PairImpl<number, number>(newCircle.getLatLng().lat, newCircle.getLatLng().lng);
                    this.mapObserver.list().forEach(c => {
                        c.onMovingNode(circle);
                        if (this.mapCM.getSecond(circle) != null) {
                            this.mapCM.getSecond(circle).setLatLng([circle.coordinates.getX(), circle.coordinates.getY()]);
                        }
                    });
                });
            }, 500);
        }
    }


    private updateFinalPosition(circle: L.Circle) {
        let c = this.mapCLC.getFirst(circle);
        this.mapObserver.list().forEach(obs => {
            obs.setFinalNodePosition(c);
        });
    }

    public addObserver(obs: MapObserver): void {
        this.mapObserver.add(obs);
    }

    public drawMap(): void {
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        this.map.addLayer(this.nodesL);
        this.map.addLayer(this.markerL);
        this.map.addLayer(this.roadL);
        this.map.addLayer(this.pathL);
    }

    public drawCircle(c: Circle): Circle {
        let newCircle = L.circle([c.coordinates.getX(), c.coordinates.getY()], {
            color: c.color,
            fillColor: c.fillColor,
            radius: c.ray,
            fillOpacity: c.opacity,
        });
        this.mapCLC.push(c, newCircle);
        newCircle.addTo(this.nodesL);
        newCircle.on("click", () => {
            this.mapObserver.list().forEach(mobs => {
                mobs.onCircleClicked(this.mapCLC.getFirst(newCircle));
            });
        });

        newCircle.on("mousedown", (event: any) => {
            this.onCircleDropped(newCircle, event);
        });

        newCircle.on("mouseup", () => {
            this.mouseUp(newCircle);
        });

        return c;
    }

    private mouseUp(circle: L.Circle): void {
        if (!this.draggableMap) {
            this.updateFinalPosition(circle);
            this.map.dragging.enable();
            this.map.removeEventListener('mousemove');
            this.map.removeEventListener('mousedown');
        }
    }

    public setDraggableMap(c: boolean) {
        this.draggableMap = c;
    }

    public drawLine(l: Line): Line {
        let coord = new Array();
        for (let i = 0; i < l.coordinates.length; i++) {
            coord[i] = ([l.coordinates[i].getX(), l.coordinates[i].getY()]);
        }

        let newLine = L.polyline(coord, {
            color: l.color,
            weight: l.weight,
            opacity: l.opacity,
        });


        this.mapLPL.push(l, newLine);
        newLine.addTo(this.map);
        newLine.addTo(this.roadL);
        newLine.on("click", () => {
            this.mapObserver.list().forEach(mobs => {
                mobs.onLineClicked(this.mapLPL.getFirst(newLine));
            });
        });
        // GESTIRE NEL CONTROLLER
        this.showItem(Item.NODE, false);
        this.showItem(Item.NODE, true);
        return l;
    }

    public changeStyle(e: any, style: Consts) {
        if ('ray' in e) {
            this.mapCLC.getSecond(e).setStyle(style);
        } else if ('weight' in e) {
            this.mapLPL.getSecond(e).setStyle(style);
        }

    }

    public showItem(i: Item, c: boolean) {
        let layer;
        switch (i) {
            case Item.NODE:
                layer = this.nodesL;
                break;
            case Item.ROAD:
                layer = this.roadL;
                break;
            default:
                layer = this.markerL;
                break;
        }

        c ? this.map.addLayer(layer) : this.map.removeLayer(layer);
    }

    public removeLine(l: Line) {
        this.roadL.removeLayer(this.mapLPL.getSecond(l));
        for (let i = 0; i < this.mapLPL.entries().size(); i++) {
            if (this.mapLPL.entries().get(i).getX() === l) {
                this.mapLPL.entries().removeIndex(i);
            }
        }
    }

    public removeCircle(c: Circle) {
        this.nodesL.removeLayer(this.mapCLC.getSecond(c));
        for (let i = 0; i < this.mapCLC.entries().size(); i++) {
            if (this.mapCLC.entries().get(i).getX() === c) {
                this.mapCLC.entries().removeIndex(i);
            }
        }
        this.removeIcon(c);
    }

    public addIconToCircle(c: Circle, path: string, size: number): void {
        let iconMarker = L.icon({
            iconUrl: this.imagePath + "/" + path + Consts.ICON_EXT,
            iconSize: [size, size],
        });

        let marker = L.marker([c.coordinates.getX(), c.coordinates.getY()]);
        marker.setIcon(iconMarker);
        this.removeIcon(c);
        marker.addTo(this.markerL);
        this.mapCM.push(c, marker);

        marker.on("click", () => {
            this.mapObserver.list().forEach(mobs => {
                mobs.onCircleClicked(this.mapCM.getFirst(marker));
                mobs.findDescription(this.mapCM.getFirst(marker));
            });
        });

        marker.on("mousedown", (event: any) => {
            this.onCircleDropped(this.mapCLC.getSecond(c), event);
        });

        marker.on("mouseup", () => {
            this.mouseUp(this.mapCLC.getSecond(c));
            this.map.dragging.enable();
            this.map.removeEventListener('mousemove');
        });
    }

    public resizeCircleIcon(c: Circle, size: number): void {
        if (this.mapCM.keys().contains(c)) {
            let marker = this.mapCM.getSecond(c);
            let oldIcon = marker.options.icon;
            let newIcon = L.icon({
                iconUrl: oldIcon.options.iconUrl,
                iconSize: [size, size],
            });
            marker.setIcon(newIcon);
        }
    }

    public removeIcon(c: Circle): void {
        if (this.mapCM.getSecond(c) != null) {
            this.markerL.removeLayer(this.mapCM.getSecond(c));
            this.mapCM.entries().indexOf(new PairImpl(c, this.mapCM.getSecond(c)));
        }
    }

    public getCircleById(id: number): Circle {
        return this.mapCLC.entries().get(id).getX();
    }

    public selectPath(lines: IList<Line>): void {
        let coord = new Array();
        this.resetPath();
        for (let i = 0; i < lines.size(); i++) {
            coord[i] = ([[lines.get(i).coordinates[0].getX(), lines.get(i).coordinates[0].getY()],
            [lines.get(i).coordinates[1].getX(), lines.get(i).coordinates[1].getY()]]);
        }
        let newLine = L.polyline(coord, {
            color: "green",
            dashArray: '20.15',
            lineJoin: 'round',
            weight: 5,
            opacity: 1,
        });
        this.pathList = newLine;
        newLine.addTo(this.map);
        newLine.addTo(this.pathL);
        this.map.addLayer(this.pathL);
    };

    public resetPath(): void {
        if (this.pathList != undefined) {
            this.pathL.removeLayer(this.pathList);
        }
    }
}