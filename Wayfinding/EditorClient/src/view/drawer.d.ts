import { Pair } from "../utility/pair";
import { Circle } from "./circle";
import { Line } from "./line";
import { MapObserver } from "../controller/map-observer";
import { Map } from "../utility/map";
import { PairImpl } from "../utility/pair-impl";
import {Consts, Item} from "../utility/consts";
import { IList } from "../utility/i-list";

export interface Drawer{
    addObserver(obs : MapObserver) : void;
    drawMap(map : any) : void;
    drawCircle(c: Circle) : Circle;
    drawLine(l: Line) : Line;
    showItem(i:Item, c:boolean) : void;
    removeLine(l: Line): void;
    removeCircle(c: Circle): void;
    addIconToCircle(c: Circle, path: string, size:number) : void;
    resizeCircleIcon(c: Circle, size : number) : void
    changeStyle(e : any, style: Consts): void;
    removeIcon(c : Circle) : void
    getCircleById(id: number): Circle ;
    setDraggableMap(c:boolean) : void;
    selectPath(lines: IList<Line>): void;
    resetPath(): void;
    onPositionUpdate(x: number, y: number): void;
}