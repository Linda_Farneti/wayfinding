import { Pair } from "../utility/pair";
import { Element } from "./element";

export interface Line extends Element{
    weight:number;
    coordinates:Array<Pair<number,number>>;
}