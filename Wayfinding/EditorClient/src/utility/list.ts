import { IList } from "./i-list";

export class List<T> implements IList<T>{

    private array : Array<T>;

    public constructor(vet?:Array<T>){
        (vet === null || vet === undefined) ?  this.array = new Array<T>() :  this.array = vet;
    }

    public contains(e: T): boolean {
        return this.array.indexOf(e) > -1;
    }
    
    public containsAll(toCheck: T[]): boolean {
        let a;
        toCheck.forEach(x => {
            this.contains(x)  ? a = true : a = false;
        });
        return a;
    }

    public get(index : number) : T{
        return this.array[index];
    }

    public add(e : T) : void{
        this.array.push(e);
    }

    public addAll(toAdd : Array<T>){
        toAdd.forEach(x => {
            this.add(x);
        });
    }

    public remove(e: T): T {
        let a = new Array<T>();
        if(this.contains(e)){
             a = this.array.splice(this.array.indexOf(e), 1);
        }
        while(this.contains(e)){
            this.array.splice(this.array.indexOf(e), 1);
        }
        return a[0];
    }

    public removeIndex(index : number) : T{
        let a  = this.get(index);
        this.array.splice(index, 1);
        return a;
    }

    public removeAll(toRemove: T[]): T[] {
        let a = new Array<T>();
        toRemove.forEach(e => {
            a.push(e);
            this.remove(e);
        });
        return a;
    }

    public replace (toRemove:T , toAdd:T) : T{
        let a = this.remove(toRemove);
        this.add(toAdd);
        return a;
    }

    public clear(): void {
        this.array = new Array<T>();
    }
    
    public list(): T[] {
       return this.array;
    }

    public size() : number{
        return this.array.length;
    }
    
    public indexOf(e : T) : number{
        return this.array.indexOf(e);
    }

    public isEmpty() : boolean{
        return this.size() === 0;
    }
    
    public removeLast(e : T) : T{
        let index:number;
        for(let i = 0; i < this.size(); i++){
            if(this.get(i) === e){
                index = i;
            }
        }
        return this.removeIndex(index);
    }
}