import { Node } from "../model/node";
import { Pair } from "./pair";

export class DistanceCalculator {
    static getDistanceNodeToNode(n1: Node, n2: Node) : number {
        let lat1 = n1.getLatitude();
        let lat2 = n2.getLatitude();
        let lon1 = n1.getLongitude();
        let lon2 = n2.getLongitude();
        var R = 6371;           // Radius of the earth  (km)
        var dLat = (lat2-lat1) * (Math.PI/180);  
        var dLon = (lon2-lon1) * (Math.PI/180);
        var a =  Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat1 * (Math.PI/180)) * Math.cos(lat2 * (Math.PI/180)) * Math.sin(dLon/2) * Math.sin(dLon/2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c;          // Distance (km)
        return d*1000;          // Distance (m)
    }

   /* static getDistanceNodeToLine(coord:Pair<number,number>, sLine:StraightLine) : number{
        return Math.abs(coord.getX() - (sLine.m*coord.getY() + sLine.q) / Math.sqrt(1 + sLine.m * sLine.m));
    }*/
}