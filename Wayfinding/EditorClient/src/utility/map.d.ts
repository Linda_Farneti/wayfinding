import { Pair } from "./pair";
import { IList } from "./i-list";

interface Map<T , K>{
    keys() : IList<T>;
    push(first:T, second:K) : void;
    getFirst(e: K): T;
    getSecond(e : T) : K;
    entries(): IList<Pair<T, K>>;
    replace(oldKey:T, newKey:T, newValue:K) : void;
}