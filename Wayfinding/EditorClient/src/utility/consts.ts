export enum State {DRAW_NODE, SELECT_PATH, SELECTION, MOVE};
export enum Item {ROAD, ICON, NODE};

export class Consts {
    static readonly CIRCLE_RAY = 5;
    static readonly MAP_FOCUS = 17;
    static readonly ROAD_WIDTH = 10;
    static readonly OPACITY = 1;
    static readonly DEFAULT_ICON_SIZE = 20;
  //  static readonly ICON_PATH = "../view/images/";
    static readonly ICON_EXT = ".svg";

}


