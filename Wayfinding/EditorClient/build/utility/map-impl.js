"use strict";
exports.__esModule = true;
var pair_impl_1 = require("./pair-impl");
var MapImpl = /** @class */ (function () {
    function MapImpl() {
        this.map = new Array();
    }
    MapImpl.prototype.keys = function () {
        var ar = new Array();
        this.map.forEach(function (x) { return ar.push(x.getX()); });
        return ar;
    };
    MapImpl.prototype.push = function (first, second) {
        this.map.push(new pair_impl_1.PairImpl(first, second));
    };
    MapImpl.prototype.getFirst = function (e) {
        var t;
        for (var i = 0; i < this.map.length; i++) {
            if (this.map[i].getY() === e) {
                t = this.map[i].getX();
            }
        }
        return t;
    };
    MapImpl.prototype.getSecond = function (e) {
        var t;
        for (var i = 0; i < this.map.length; i++) {
            if (this.map[i].getX() === e) {
                t = this.map[i].getY();
            }
        }
        return t;
    };
    return MapImpl;
}());
exports.MapImpl = MapImpl;
