"use strict";
exports.__esModule = true;
var List = /** @class */ (function () {
    function List(vet) {
        (vet === null || vet === undefined) ? this.array = new Array() : this.array = vet;
    }
    List.prototype.contains = function (e) {
        return this.array.indexOf(e) > -1;
    };
    List.prototype.containsAll = function (toCheck) {
        var _this = this;
        var a;
        toCheck.forEach(function (x) {
            _this.contains(x) ? a = true : a = false;
        });
        return a;
    };
    List.prototype.get = function (index) {
        return this.array[index];
    };
    List.prototype.add = function (e) {
        this.array.push(e);
    };
    List.prototype.addAll = function (toAdd) {
        var _this = this;
        toAdd.forEach(function (x) {
            _this.add(x);
        });
    };
    List.prototype.remove = function (e) {
        var a = new Array();
        if (this.contains(e)) {
            a = this.array.splice(this.array.indexOf(e), 1);
        }
        while (this.contains(e)) {
            this.array.splice(this.array.indexOf(e), 1);
        }
        return a[0];
    };
    List.prototype.removeIndex = function (index) {
        var a = this.get(index);
        this.array.splice(index, 1);
        return a;
    };
    List.prototype.removeAll = function (toRemove) {
        var _this = this;
        var a = new Array();
        toRemove.forEach(function (e) {
            a.push(e);
            _this.remove(e);
        });
        return a;
    };
    List.prototype.replace = function (toRemove, toAdd) {
        var a = this.remove(toRemove);
        this.add(toAdd);
        return a;
    };
    List.prototype.clear = function () {
        this.array = new Array();
    };
    List.prototype.list = function () {
        return this.array;
    };
    List.prototype.size = function () {
        return this.array.length;
    };
    List.prototype.indexOf = function (e) {
        return this.array.indexOf(e);
    };
    List.prototype.isEmpty = function () {
        return this.size() === 0;
    };
    List.prototype.removeLast = function (e) {
        var index;
        for (var i = 0; i < this.size(); i++) {
            if (this.get(i) === e) {
                index = i;
            }
        }
        return this.removeIndex(index);
    };
    return List;
}());
exports.List = List;
