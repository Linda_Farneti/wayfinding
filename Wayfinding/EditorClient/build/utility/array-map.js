"use strict";
exports.__esModule = true;
var pair_impl_1 = require("./pair-impl");
var list_1 = require("./list");
var ArrayMap = /** @class */ (function () {
    function ArrayMap() {
        this.map = new list_1.List();
    }
    ArrayMap.prototype.keys = function () {
        var ar = new list_1.List();
        this.map.list().forEach(function (x) { return ar.add(x.getX()); });
        return ar;
    };
    ArrayMap.prototype.push = function (first, second) {
        this.map.list().push(new pair_impl_1.PairImpl(first, second));
    };
    ArrayMap.prototype.getFirst = function (e) {
        var t;
        for (var i = 0; i < this.map.list().length; i++) {
            if (this.map.get(i).getY() === e) {
                t = this.map.get(i).getX();
            }
        }
        return t;
    };
    ArrayMap.prototype.getSecond = function (e) {
        var t;
        for (var i = 0; i < this.map.list().length; i++) {
            if (this.map.get(i).getX() === e) {
                t = this.map.get(i).getY();
            }
        }
        return t;
    };
    ArrayMap.prototype.entries = function () {
        return this.map;
    };
    ArrayMap.prototype.replace = function (oldKey, newKey, newValue) {
        var _this = this;
        this.map.list().forEach(function (p) {
            if (p.getX() === oldKey) {
                _this.map.remove(p);
            }
        });
        this.map.add(new pair_impl_1.PairImpl(newKey, newValue));
    };
    return ArrayMap;
}());
exports.ArrayMap = ArrayMap;
