"use strict";
exports.__esModule = true;
var State;
(function (State) {
    State[State["DRAW_NODE"] = 0] = "DRAW_NODE";
    State[State["SELECT_PATH"] = 1] = "SELECT_PATH";
    State[State["SELECTION"] = 2] = "SELECTION";
    State[State["MOVE"] = 3] = "MOVE";
})(State = exports.State || (exports.State = {}));
;
var Item;
(function (Item) {
    Item[Item["ROAD"] = 0] = "ROAD";
    Item[Item["ICON"] = 1] = "ICON";
    Item[Item["NODE"] = 2] = "NODE";
})(Item = exports.Item || (exports.Item = {}));
;
var Consts = /** @class */ (function () {
    function Consts() {
    }
    Consts.CIRCLE_RAY = 5;
    Consts.MAP_FOCUS = 17;
    Consts.ROAD_WIDTH = 10;
    Consts.OPACITY = 1;
    Consts.DEFAULT_ICON_SIZE = 20;
    //  static readonly ICON_PATH = "../view/images/";
    Consts.ICON_EXT = ".svg";
    return Consts;
}());
exports.Consts = Consts;
