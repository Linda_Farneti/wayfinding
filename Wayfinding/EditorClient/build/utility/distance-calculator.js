"use strict";
exports.__esModule = true;
var DistanceCalculator = /** @class */ (function () {
    function DistanceCalculator() {
    }
    DistanceCalculator.getDistanceNodeToNode = function (n1, n2) {
        var lat1 = n1.getLatitude();
        var lat2 = n2.getLatitude();
        var lon1 = n1.getLongitude();
        var lon2 = n2.getLongitude();
        var R = 6371; // Radius of the earth  (km)
        var dLat = (lat2 - lat1) * (Math.PI / 180);
        var dLon = (lon2 - lon1) * (Math.PI / 180);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * (Math.PI / 180)) * Math.cos(lat2 * (Math.PI / 180)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance (km)
        return d * 1000; // Distance (m)
    };
    return DistanceCalculator;
}());
exports.DistanceCalculator = DistanceCalculator;
