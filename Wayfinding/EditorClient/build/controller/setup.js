"use strict";
exports.__esModule = true;
var L = require("leaflet");
var graph_creator_1 = require("../model/graph-creator");
var consts_1 = require("../utility/consts");
var list_1 = require("../utility/list");
var map_drawer_1 = require("../view/map-drawer");
var browser_observable_1 = require("./browser-observable");
var controller_1 = require("./controller");
var icon_manager_1 = require("./icon-manager");
var settings_manager_1 = require("./settings-manager");
var Setup = /** @class */ (function () {
    function Setup() {
    }
    Setup.prototype.setup = function () {
        var _this = this;
        this.getMapInfo(function (longitude, latitude, zoom, name) {
            _this.getIcons(function () {
                _this.initComponents(longitude, latitude, zoom);
            });
            $("#mapname > h1").text(name);
        });
    };
    Setup.prototype.getMapInfo = function (callback) {
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Map/GetMapFromID?ID=" + MapId,
            contentType: "application/json",
            data: null,
            success: function (data) {
                callback(data.Longitude, data.Latitude, data.Zoom, data.Name);
            },
            dataType: "json"
        });
    };
    Setup.prototype.getIcons = function (callback) {
        icon_manager_1.IconManager.getInstance().getIconsDB(function (icons) {
            icons.list().forEach(function (i) {
                var r = $('<input/>').attr({
                    "class": "icons",
                    type: "image",
                    src: ImagePath + "/" + i + consts_1.Consts.ICON_EXT,
                    name: i,
                    width: "40",
                    height: "40",
                    title: i
                });
                $("#iconsList").append(r);
            });
            callback();
        });
    };
    Setup.prototype.initComponents = function (Longitude, Latitude, Zoom) {
        var _this = this;
        var b = browser_observable_1.BrowserObservable.getInstance();
        var sm = settings_manager_1.SettingsManager.getInstance();
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Settings/GetColors",
            contentType: "application/json",
            data: null,
            success: function (i) {
                sm.setCircleC(i[0]);
                sm.setCircleBC(i[1]);
                sm.setRoadC(i[2]);
                sm.setRoadSSC(i[3]);
                sm.setCircleSSC(i[4]);
            },
            dataType: "json"
        });
        var mymap = L.map('mymap').setView([Latitude, Longitude], Zoom);
        this.graph = new graph_creator_1.GraphCreator(MapId);
        this.drawers = new list_1.List(new Array(new map_drawer_1.MapDrawer(mymap, ImagePath)));
        this.controller = new controller_1.Controller(this.drawers, this.graph);
        this.drawers.list().forEach(function (d) {
            d.addObserver(_this.controller);
        });
        b.addObserver(this.controller);
        b.setMap(mymap);
        b.setup();
        this.controller.loadMap(MapId);
    };
    return Setup;
}());
$(document).ready(function () {
    new Setup().setup();
});
