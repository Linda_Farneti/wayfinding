"use strict";
exports.__esModule = true;
var consts_1 = require("../utility/consts");
var list_1 = require("../utility/list");
var BrowserObservable = /** @class */ (function () {
    function BrowserObservable() {
        this.observers = new list_1.List();
    }
    BrowserObservable.showDescription = function (text) {
        $("#description").show();
        $("#description_t").val(text);
    };
    BrowserObservable.prototype.setup = function () {
        var _this = this;
        var hide = true;
        var hideEdit = true;
        $("#options").hide();
        $("#viewoptions").click(function () {
            $("#options").slideToggle();
            if (hide) {
                $("#viewoptions > img").attr('src', "Content/WebImages/arrowup.svg");
            }
            else {
                $("#viewoptions > img").attr('src', "Content/WebImages/arrowdown.svg");
            }
            hide = !hide;
        });
        $("#edit").hide();
        $("#description").hide();
        $("#dtp").click(function () {
            $("#description").fadeToggle();
        });
        //   $("#load_gif").show();
        $("#nodeLayer").prop("checked", true);
        $("#iconLayer").prop("checked", true);
        $("#roadLayer").prop("checked", true);
        $("#select").click(function () {
            //$("#load_gif").show();
            _this.observers.list().forEach(function (o) {
                o.setState(consts_1.State.SELECTION);
            });
            if (hideEdit) {
                $("#select > label").text("Close Editor");
                $("#mymap").css("margin-left", "400px");
            }
            else {
                $("#select > label").text("Open Editor");
                $("#mymap").css("margin-left", "250px");
            }
            hideEdit = !hideEdit;
            $("#edit").fadeToggle("fast");
        });
        $("#setDescription").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onSetDescription($("#description_t").val());
            });
            $("#description_t").val("");
        });
        $("#drawNode").click(function () {
            _this.observers.list().forEach(function (o) {
                o.setState(consts_1.State.DRAW_NODE);
            });
        });
        $("#drawRoad").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onDrawRoad($("#roadSize").val());
            });
        });
        $("#selectPath").click(function () {
            _this.observers.list().forEach(function (o) {
                o.setState(consts_1.State.SELECT_PATH);
                o.onSelectPath();
            });
        });
        $("#delete").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onDeleteMap();
            });
        });
        $("#nodeLayer").change(function () {
            var c = $("#nodeLayer").is(':checked');
            _this.observers.list().forEach(function (o) {
                o.onShowItem(consts_1.Item.NODE, c);
            });
        });
        $("#iconLayer").change(function () {
            var c = $("#iconLayer").is(':checked');
            _this.observers.list().forEach(function (o) {
                o.onShowItem(consts_1.Item.ICON, c);
            });
        });
        $("#roadSize").mousedown(function () {
            $("#roadSize").mousemove(function () {
                _this.observers.list().forEach(function (o) {
                    o.onUpdateRoadSize($("#roadSize").val());
                });
            });
        });
        $("#roadSize").mouseup(function () {
            _this.observers.list().forEach(function (o) {
                o.setFinalRoadSize($("#roadSize").val());
            });
        });
        $("#resize").mousedown(function () {
            $("#resize").mousemove(function () {
                _this.observers.list().forEach(function (o) {
                    o.onResizeViewIcons($("#resize").val());
                });
            });
        });
        $("#resize").mouseup(function () {
            _this.observers.list().forEach(function (o) {
                o.onResizeModelIcons($("#resize").val());
            });
        });
        $("#resize").mouseup(function () {
            $("#resize").unbind("mousemove");
        });
        $("#roadLayer").change(function () {
            var c = $("#roadLayer").is(':checked');
            _this.observers.list().forEach(function (o) {
                o.onShowItem(consts_1.Item.ROAD, c);
            });
        });
        $("#remove").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onRemoveElements();
            });
        });
        $("#removeI").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onRemoveIcons(function () { });
            });
        });
        var a;
        $(".icons").click(function () {
            a = $(this);
        });
        $(".icons").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onAddIcons(a.attr('name'));
            });
        });
        $("#move").click(function () {
            _this.observers.list().forEach(function (o) {
                o.setState(consts_1.State.MOVE);
            });
        });
        /*   $("#export").click(() => {
               this.observers.list().forEach(o => {
                      $("#JSONfile").val(o.onExportJson());
               });
           });
   
           $("#import").click(() => {
               this.observers.list().forEach(o => {
                   o.onImportJson(<string>$("#JSONfile").val());
               });
           });*/
        $("#selectPath").click(function () {
            _this.observers.list().forEach(function (o) {
                o.onSelectPath();
            });
        });
        /* $("#start").click(() => {
             this.observers.list().forEach(o => {
                 o.onStart();
             });
         });*/
    };
    BrowserObservable.prototype.addObserver = function (obs) {
        this.observers.add(obs);
    };
    BrowserObservable.prototype.getObservers = function () {
        return this.observers;
    };
    BrowserObservable.prototype.setMap = function (map) {
        this.map = map;
        (this.map).on("click", function (e) {
            BrowserObservable.getInstance().getObservers().list().forEach(function (obs) {
                obs.onMapClicked(e);
            });
        });
    };
    BrowserObservable.getInstance = function () {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new BrowserObservable();
        }
        return this.instance;
    };
    return BrowserObservable;
}());
exports.BrowserObservable = BrowserObservable;
