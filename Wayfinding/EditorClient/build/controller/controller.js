"use strict";
exports.__esModule = true;
var array_map_1 = require("../utility/array-map");
var consts_1 = require("../utility/consts");
var distance_calculator_1 = require("../utility/distance-calculator");
var list_1 = require("../utility/list");
var pair_impl_1 = require("../utility/pair-impl");
var browser_observable_1 = require("./browser-observable");
var settings_manager_1 = require("./settings-manager");
var Controller = /** @class */ (function () {
    function Controller(ds, g) {
        this.sm = settings_manager_1.SettingsManager.getInstance();
        this.nodesClicked = new list_1.List();
        this.deselectedNodes = new list_1.List();
        this.roadsClicked = new list_1.List();
        this.maps = new list_1.List();
        this.mapCN = new array_map_1.ArrayMap();
        this.mapLR = new array_map_1.ArrayMap();
        this.drawers = ds;
        this.graph = g;
        this.actualStyle = this.sm.CircleC;
    }
    Controller.prototype.canSelectNode = function (node) {
        var _this = this;
        if ((this.nodesClicked.size() === 0) || (this.nodesClicked.get(this.nodesClicked.list().length - 1) !== node)) {
            return true;
        }
        else {
            if (this.nodesClicked.get(this.nodesClicked.list().length - 1) === node) {
                this.drawers.list().forEach(function (d) {
                    d.changeStyle(_this.mapCN.getFirst(node), _this.sm.CircleBC);
                });
                this.nodesClicked.removeLast(node);
                this.deselectedNodes.add(node);
            }
        }
        return false;
    };
    Controller.prototype.updateEdge = function (r, size, poly) {
        var _this = this;
        var l = this.mapLR.getFirst(r);
        var newLine;
        this.drawers.list().forEach(function (d) {
            d.removeLine(l);
        });
        for (var i = 0; i < this.mapLR.entries().size(); i++) {
            if (this.mapLR.entries().get(i).getY() === r) {
                this.mapLR.entries().removeIndex(i);
            }
        }
        this.drawers.list().forEach(function (d) {
            newLine = { color: _this.sm.RoadC.color, coordinates: poly, fillColor: _this.sm.RoadC.color, opacity: consts_1.Consts.OPACITY, weight: size };
            d.drawLine(newLine);
            d.changeStyle(newLine, _this.sm.RoadSSC);
        });
        this.mapLR.push(newLine, r);
    };
    Controller.prototype.removeEdge = function (r) {
        var _this = this;
        //console.log("removing edge...");
        //let l = this.mapLR.getFirst(r);
        var l = null;
        this.mapLR.entries().list().forEach(function (x) {
            if (x.getY().getName() === r.getName()) {
                l = x.getX();
            }
        });
        this.drawers.list().forEach(function (d) {
            // console.log("removing line : " + l);
            d.removeLine(l);
        });
        this.graph.removeRoad(r, function () {
            //  console.log("removing road : " + r);
            for (var i = 0; i < _this.mapLR.entries().size(); i++) {
                if (_this.mapLR.entries().get(i).getY() === r) {
                    _this.mapLR.entries().removeIndex(i);
                }
            }
        });
    };
    Controller.prototype.createRoad = function (n1, n2, size) {
        var polylinePoints;
        polylinePoints = new Array(n1.getCoordinates(), n2.getCoordinates());
        var r = this.graph.getRoadBetweenNodes(n1, n2);
        if (r === null || r === undefined) {
            this.addRoad(polylinePoints, n1, n2, size);
            //       console.log("finish function add road controller");
        }
        else {
            if (r.getSize() !== size) {
                this.removeEdge(r);
                this.addRoad(polylinePoints, n1, n2, size);
            }
        }
        this.nodeInForeground();
    };
    Controller.prototype.convertToPair = function (e) {
        var s = e.latlng.toString();
        var ar = s.substring(7, s.length - 1).split(",");
        return new pair_impl_1.PairImpl(parseFloat(ar[0]), parseFloat(ar[1]));
    };
    Controller.prototype.resetClickedItems = function () {
        var _this = this;
        this.drawers.list().forEach(function (d) {
            _this.nodesClicked.list().forEach(function (n) {
                d.changeStyle(_this.mapCN.getFirst(n), _this.sm.CircleBC);
            });
            _this.roadsClicked.list().forEach(function (r) {
                d.changeStyle(_this.mapLR.getFirst(r), _this.sm.RoadC);
            });
            d.setDraggableMap(true);
        });
        this.nodesClicked.clear();
        this.roadsClicked.clear();
        this.selection = consts_1.State.SELECTION;
    };
    Controller.prototype.removeNode = function (n) {
        var c = this.mapCN.getFirst(n);
        this.mapCN.entries().remove(new pair_impl_1.PairImpl(c, n));
        this.graph.removeNode(n);
        this.drawers.list().forEach(function (d) {
            d.removeCircle(c);
        });
    };
    Controller.prototype.createNode = function (coord) {
        var _this = this;
        if (this.graph.positionAvailableForNodes(coord)) {
            this.graph.addNode(coord, function (n) {
                var c;
                _this.drawers.list().forEach(function (d) {
                    c = { color: _this.sm.CircleBC.color, coordinates: coord, fillColor: _this.sm.CircleC.color, opacity: consts_1.Consts.OPACITY, ray: consts_1.Consts.CIRCLE_RAY };
                    c = d.drawCircle(c);
                });
                _this.mapCN.push(c, n);
            });
        }
    };
    Controller.prototype.addRoad = function (poly, n1, n2, size) {
        var _this = this;
        this.graph.addRoad(n1, n2, size, function (r) {
            var l;
            _this.drawers.list().forEach(function (d) {
                l = { color: _this.sm.RoadC.color, coordinates: poly, fillColor: _this.sm.RoadC.color, opacity: consts_1.Consts.OPACITY, weight: size };
                d.drawLine(l);
            });
            _this.mapLR.push(l, r);
        });
    };
    Controller.prototype.nodeInForeground = function () {
        this.drawers.list().forEach(function (d) {
            d.showItem(consts_1.Item.NODE, false);
            d.showItem(consts_1.Item.NODE, true);
        });
    };
    Controller.prototype.loadMap = function (id) {
        var _this = this;
        var i = id; // useless
        this.graph.loadNodes(function (nodes) {
            nodes.list().forEach(function (n) {
                var c;
                _this.drawers.list().forEach(function (d) {
                    c = { color: _this.sm.CircleBC.color, coordinates: n.getCoordinates(), fillColor: _this.sm.CircleC.color, opacity: consts_1.Consts.OPACITY, ray: consts_1.Consts.CIRCLE_RAY };
                    c = d.drawCircle(c);
                });
                _this.mapCN.push(c, n);
            });
            _this.graph.loadRoads(function (roads) {
                roads.list().forEach(function (r) {
                    var l;
                    _this.drawers.list().forEach(function (d) {
                        l = { color: _this.sm.RoadC.color, coordinates: [r.getCoupleNodes().getX().getCoordinates(), r.getCoupleNodes().getY().getCoordinates()], fillColor: "white", opacity: consts_1.Consts.OPACITY, weight: r.getSize() };
                        d.drawLine(l);
                    });
                    _this.mapLR.push(l, r);
                });
            });
            _this.graph.loadPois(function (points) {
                points.list().forEach(function (p) {
                    _this.drawers.list().forEach(function (d) {
                        var c = _this.mapCN.getFirst(_this.graph.getNodeById(p.nodeID));
                        d.addIconToCircle(c, p.path, p.size);
                    });
                });
            });
        });
    };
    Controller.prototype.setState = function (newState) {
        this.selection = newState;
        if (this.selection === consts_1.State.MOVE) {
            this.drawers.list().forEach(function (d) {
                d.setDraggableMap(false);
            });
        }
        else {
            this.drawers.list().forEach(function (d) {
                d.setDraggableMap(true);
            });
        }
        if (this.selection === consts_1.State.DRAW_NODE) {
            this.actualStyle = this.sm.CircleSSC;
        }
        this.drawers.list().forEach(function (d) {
            d.resetPath();
        });
    };
    /* STRADE RIPETUTE SE DISEGNATE ALLO STESSO MOMENTO                                                 PROBLEM */
    Controller.prototype.onDrawRoad = function (size) {
        var n1, n2;
        if (this.nodesClicked.list().length >= 2) {
            for (var i = 0; i < this.nodesClicked.list().length - 1; i++) {
                n1 = this.nodesClicked.get(i);
                n2 = this.nodesClicked.get(i + 1);
                this.createRoad(n1, n2, size);
            }
        }
        this.resetClickedItems();
    };
    Controller.prototype.onDeleteMap = function () {
        this.graph.deleteMap();
    };
    Controller.prototype.onSetDescription = function (text) {
        var _this = this;
        console.log(text);
        this.nodesClicked.list().forEach(function (n) {
            _this.graph.onDescriptionSet(n.getId(), text);
        });
        this.resetClickedItems();
    };
    Controller.prototype.onUpdateRoadSize = function (newSize) {
        var _this = this;
        this.roadsClicked.list().forEach(function (r) {
            var polyline = new Array(r.getCoupleNodes().getX().getCoordinates(), r.getCoupleNodes().getY().getCoordinates());
            _this.updateEdge(r, newSize, polyline);
        });
    };
    Controller.prototype.onShowItem = function (i, c) {
        this.drawers.list().forEach(function (d) {
            d.showItem(i, c);
        });
        if (i === consts_1.Item.ROAD && c) {
            this.nodeInForeground();
        }
    };
    Controller.prototype.removeConnectedItems = function (callback) {
        var _this = this;
        var roads = new list_1.List();
        this.roadsClicked.list().forEach(function (r) { _this.removeEdge(r); });
        this.nodesClicked.list().forEach(function (n) {
            if ((_this.mapCN.getFirst(n) != null) && _this.nodesClicked.contains(n)) {
                roads.addAll(_this.graph.getRoadsFromNode(n).list());
                roads.list().forEach(function (r) {
                    if (!_this.roadsClicked.contains(r)) {
                        _this.removeEdge(r);
                    }
                });
                _this.drawers.list().forEach(function (d) { d.removeIcon(_this.mapCN.getFirst(n)); });
                _this.graph.removePoint(n.getId());
                roads.clear();
            }
        });
        callback();
    };
    Controller.prototype.onRemoveElements = function () {
        var _this = this;
        this.removeConnectedItems(function () {
            _this.nodesClicked.list().forEach(function (n) {
                if ((_this.mapCN.getFirst(n) != null) && _this.nodesClicked.contains(n)) {
                    _this.removeNode(n);
                }
            });
        });
        this.roadsClicked.clear();
        this.nodesClicked.clear();
    };
    Controller.prototype.onRemoveIcons = function (callback) {
        var _this = this;
        this.nodesClicked.list().forEach(function (n) {
            if (_this.nodesClicked.contains(n) && !_this.deselectedNodes.contains(n)) {
                _this.drawers.list().forEach(function (d) {
                    d.removeIcon(_this.mapCN.getFirst(n));
                });
                _this.graph.removePoint(n.getId());
            }
        });
        callback();
        this.resetClickedItems();
        this.deselectedNodes.clear();
    };
    Controller.prototype.onAddIcons = function (s) {
        var _this = this;
        var savedClickedNodes = this.nodesClicked.list();
        this.onRemoveIcons(function () {
            savedClickedNodes.forEach(function (n) {
                if (!_this.deselectedNodes.contains(n)) {
                    _this.graph.addIconToPoint(s, n.getId(), consts_1.Consts.DEFAULT_ICON_SIZE, "", s, function () {
                        var c = _this.mapCN.getFirst(n);
                        _this.drawers.list().forEach(function (d) {
                            d.addIconToCircle(c, s, consts_1.Consts.DEFAULT_ICON_SIZE);
                        });
                    });
                }
            });
            _this.deselectedNodes.clear();
        });
    };
    Controller.prototype.onResizeViewIcons = function (newSize) {
        var _this = this;
        this.drawers.list().forEach(function (d) {
            _this.nodesClicked.list().forEach(function (nc) {
                d.resizeCircleIcon(_this.mapCN.getFirst(nc), newSize);
            });
        });
    };
    Controller.prototype.onResizeModelIcons = function (newSize) {
        var _this = this;
        this.nodesClicked.list().forEach(function (nc) {
            _this.graph.resizePointIcon(nc.getId(), newSize);
        });
    };
    Controller.prototype.onMapClicked = function (e) {
        var pc = this.convertToPair(e);
        if (this.selection === consts_1.State.DRAW_NODE) {
            this.createNode(pc);
        }
    };
    Controller.prototype.onExportJson = function () {
        return this.graph.setJSON();
    };
    Controller.prototype.onCircleClicked = function (circle) {
        var _this = this;
        var node = this.mapCN.getSecond(circle);
        if (this.canSelectNode(node)) {
            this.nodesClicked.add(node);
            this.drawers.list().forEach(function (d) {
                d.changeStyle(circle, _this.sm.CircleSSC);
            });
        }
    };
    Controller.prototype.onLineClicked = function (line) {
        var _this = this;
        var road = this.mapLR.getSecond(line);
        if (this.roadsClicked.list().indexOf(road) < 0) {
            this.roadsClicked.add(road);
            this.drawers.list().forEach(function (d) {
                d.changeStyle(line, _this.sm.RoadSSC);
            });
        }
        else {
            this.drawers.list().forEach(function (d) {
                d.changeStyle(line, _this.sm.RoadC);
            });
            this.roadsClicked.removeIndex(this.roadsClicked.list().indexOf(road));
        }
    };
    Controller.prototype.onSelectPath = function () {
        var _this = this;
        var path;
        var drawPath = new list_1.List();
        this.mapLR.keys().list().forEach(function (l) {
            _this.drawers.list().forEach(function (d) {
                d.changeStyle(l, _this.sm.RoadC);
            });
        });
        if (this.nodesClicked.size() >= 2) {
            this.graph.getPath(this.nodesClicked.get(0), this.nodesClicked.get(1), function (list) {
                list.list().forEach(function (x) {
                    _this.mapLR.entries().list().forEach(function (lr) {
                        if (lr.getY().getName() == x) {
                            var l = lr.getX();
                            drawPath.add(l);
                        }
                    });
                });
                _this.drawers.list().forEach(function (d) {
                    d.selectPath(drawPath);
                });
            });
        }
        this.resetClickedItems();
    };
    Controller.prototype.onMovingNode = function (circle) {
        var _this = this;
        if (this.selection === consts_1.State.MOVE) {
            var movingNode_1 = this.mapCN.getSecond(circle);
            movingNode_1.setCoordinates(circle.coordinates);
            this.graph.getRoadsFromNode(movingNode_1).list().forEach(function (r) {
                var l = _this.mapLR.getFirst(r);
                var oldSize;
                _this.mapLR.entries().list().forEach(function (x) {
                    if (x.getY().getName() === r.getName()) {
                        l = x.getX();
                    }
                });
                _this.drawers.list().forEach(function (d) {
                    oldSize = l.weight;
                    d.removeLine(l);
                });
                for (var i = 0; i < _this.mapLR.entries().size(); i++) {
                    if (_this.mapLR.entries().get(i).getY() === r) {
                        _this.mapLR.entries().removeIndex(i);
                    }
                }
                var otherNode = r.getCoupleNodes().getX() !== movingNode_1 ? r.getCoupleNodes().getX() : r.getCoupleNodes().getY();
                var polylinePoints = new Array(movingNode_1.getCoordinates(), otherNode.getCoordinates());
                _this.drawers.list().forEach(function (d) {
                    l = { color: _this.sm.RoadC.color, coordinates: polylinePoints, fillColor: _this.sm.RoadC.color, opacity: consts_1.Consts.OPACITY, weight: oldSize };
                    d.drawLine(l);
                });
                _this.mapLR.push(l, r);
            });
        }
    };
    Controller.prototype.setFinalNodePosition = function (circle) {
        var _this = this;
        var node = this.mapCN.getSecond(circle);
        node.setCoordinates(circle.coordinates);
        this.graph.updateNodePosition(node.getId(), circle.coordinates.getX(), circle.coordinates.getY());
        this.graph.getRoadsFromNode(node).list().forEach(function (r) {
            var otherNode = r.getCoupleNodes().getX() !== node ? r.getCoupleNodes().getX() : r.getCoupleNodes().getY();
            var newLength = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(node, otherNode);
            _this.graph.updateRoad(r, newLength, undefined);
        });
    };
    Controller.prototype.setFinalRoadSize = function (finalSize) {
        var _this = this;
        this.roadsClicked.list().forEach(function (r) {
            _this.graph.updateRoad(r, undefined, finalSize);
            _this.drawers.list().forEach(function (d) {
                d.changeStyle(_this.mapLR.getFirst(r), _this.sm.RoadC);
            });
        });
        this.roadsClicked.clear();
    };
    Controller.prototype.findDescription = function (c) {
        var n = this.mapCN.getSecond(c);
        browser_observable_1.BrowserObservable.showDescription(this.graph.getPointDescription(n.getId()));
    };
    return Controller;
}());
exports.Controller = Controller;
