"use strict";
exports.__esModule = true;
var list_1 = require("../utility/list");
var IconManager = /** @class */ (function () {
    function IconManager() {
    }
    IconManager.prototype.getIconsDB = function (callback) {
        var icons = new list_1.List();
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Icon/GetIcons",
            contentType: "application/json",
            success: function (i) {
                var a = new Array(i);
                for (var index = 0; index < a[0].length; index++) {
                    icons.add(a[0][index].Name);
                }
                $("#load_gif").hide();
                callback(icons);
            },
            dataType: "json"
        });
    };
    IconManager.getInstance = function () {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new IconManager();
        }
        return this.instance;
    };
    return IconManager;
}());
exports.IconManager = IconManager;
