"use strict";
exports.__esModule = true;
var SettingsManager = /** @class */ (function () {
    function SettingsManager() {
        this.CircleC = {
            color: "grey"
        };
        this.RoadC = {
            color: "white"
        };
        this.CircleBC = {
            color: "grey"
        };
        this.RoadSSC = {
            color: "blue"
        };
        this.CircleSSC = {
            color: "blue"
        };
    }
    SettingsManager.prototype.SettingsManager = function () { };
    SettingsManager.prototype.setCircleC = function (color) {
        this.CircleC = {
            color: color
        };
    };
    SettingsManager.prototype.setRoadC = function (color) {
        this.RoadC = {
            color: color
        };
    };
    SettingsManager.prototype.setCircleBC = function (color) {
        this.CircleBC = {
            color: color
        };
    };
    SettingsManager.prototype.setRoadSSC = function (color) {
        this.RoadSSC = {
            color: color
        };
    };
    SettingsManager.prototype.setCircleSSC = function (color) {
        this.CircleSSC = {
            color: color
        };
    };
    SettingsManager.getInstance = function () {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new SettingsManager();
        }
        return this.instance;
    };
    return SettingsManager;
}());
exports.SettingsManager = SettingsManager;
