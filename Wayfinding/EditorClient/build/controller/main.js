"use strict";
exports.__esModule = true;
var map_drawer_1 = require("../view/map-drawer");
var graph_creator_1 = require("../model/graph-creator");
var array_map_1 = require("../utility/array-map");
var leaf_circle_1 = require("../view/leaf-circle");
var leaf_line_1 = require("../view/leaf-line");
var L = require("leaflet");
var pair_impl_1 = require("../utility/pair-impl");
$(document).ready(function () {
    var state;
    (function (state) {
        state[state["SET_NODE"] = 0] = "SET_NODE";
        state[state["SET_ROAD"] = 1] = "SET_ROAD";
        state[state["SELECT_PATH"] = 2] = "SELECT_PATH";
    })(state || (state = {}));
    ;
    var selected = state.SET_NODE;
    var drawer_obj;
    var graph_obj = new graph_creator_1.GraphCreator();
    var nodesClicked = new Array();
    var mapCN = new array_map_1.ArrayMap();
    var mapPR = new array_map_1.ArrayMap();
    //SEA: 44.418848, 12.865416
    //VARIGNANA: 44.40262, 11.50949
    var mymap = L.map('mymap').setView([44.418848, 12.865416], 17);
    drawer_obj = new map_drawer_1.MapDrawer(mymap);
    $("#setCircle").click(function () {
        selected = state.SET_NODE;
    });
    $("#setPolygon").click(function () {
        selected = state.SET_ROAD;
    });
    $("#info").click(function () {
        selected = state.SELECT_PATH;
    });
    function onMapClick(e) {
        var p = convertToPair(e);
        switch (selected) {
            case state.SET_NODE:
                createNode(p);
                break;
            case state.SET_ROAD:
                if (canSelectNode(p)) {
                    if (saveClickedNodes()) {
                        createRoad();
                    }
                }
                break;
            case state.SELECT_PATH:
                if (canSelectNode(e.latlng)) {
                    if (saveClickedNodes()) {
                        // PathCreator.getPath(nodesClicked[0], nodesClicked[1], graph_obj.getRoads())
                        // drawer_obj.pathPolygon(mapPR.getFirst(<GraphRoad>graph_obj.getRoads()[0]));
                        // bisogna far tornare i nodi del loro colore
                    }
                }
                break;
            default: break;
        }
    }
    mymap.on('click', onMapClick);
    function createNode(coord) {
        if (graph_obj.positionAvailable(coord)) {
            mapCN.push(drawer_obj.drawCircle(new leaf_circle_1.LeafCircle(coord, 'white', 'grey', 10, 1)), graph_obj.addNode(graph_obj.getNodeIndex(), coord));
        }
        /*let n = new GraphNode(graph_obj.getNodeIndex(), coord);
        let c = L.circle(coord, {
            color: "grey",
            fillColor: "white",
            fillOpacity: 5,
            radius: 10,
        });
        c.on("click", function(){
            console.log((<GraphNode>mapCN.getSecond(c)).toString());
        });
        if(graph_obj.addNode(n)){
            drawer_obj.drawCircle(c);
            mapCN.push(c,n);
        }*/
    }
    function createRoad() {
        if (!graph_obj.roadExist(nodesClicked[0], nodesClicked[1])) {
            var polylinePoints = new Array(nodesClicked[0].getCoordinates(), nodesClicked[1].getCoordinates());
            console.log();
            mapPR.push(drawer_obj.drawLine(new leaf_line_1.LeafLine(polylinePoints, 10, "white", "white", 1)), graph_obj.addRoad(graph_obj.getRoadIndex(), nodesClicked[0], nodesClicked[1]));
            nodesClicked = new Array();
        }
    }
    function canSelectNode(p) {
        if (!graph_obj.positionAvailable(p)) {
            if ((nodesClicked.length === 0) || (nodesClicked[0] !== graph_obj.getSelectedNode())) {
                return true;
            }
        }
        else {
            nodesClicked = new Array();
        }
        return false;
    }
    function saveClickedNodes() {
        nodesClicked.push(graph_obj.getSelectedNode());
        /* if (selected === state.SELECT_PATH) {
             drawer_obj.pathCircle(mapCN.getFirst(graph_obj.getSelectedNode()));
         } else {
             drawer_obj.highlightCircle(mapCN.getFirst(graph_obj.getSelectedNode()));
         }*/
        // MANCA LO STILE!!!
        if (nodesClicked.length >= 2) {
            //nodesClicked.forEach(element => {
            //drawer_obj.defaultCircle(mapCN.getFirst(element));
            //});
            return true;
        }
        return false;
    }
    function convertToPair(e) {
        var s = e.latlng.toString();
        var ar = s.substring(7, s.length - 1).split(",");
        return new pair_impl_1.PairImpl(parseFloat(ar[0]), parseFloat(ar[1]));
    }
});
