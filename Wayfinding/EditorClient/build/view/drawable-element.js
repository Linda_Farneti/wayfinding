"use strict";
exports.__esModule = true;
var consts_1 = require("../utility/consts");
var DrawableElement = /** @class */ (function () {
    function DrawableElement(fC, c, o) {
        this.fillColor = 'white';
        this.color = 'white';
        this.opacity = consts_1.Consts.OPACITY;
        this.fillColor = fC;
        this.color = c;
        this.opacity = o;
    }
    DrawableElement.prototype.getFillColor = function () {
        return this.fillColor;
    };
    DrawableElement.prototype.getColor = function () {
        return this.color;
    };
    DrawableElement.prototype.getOpacity = function () {
        return this.opacity;
    };
    return DrawableElement;
}());
exports.DrawableElement = DrawableElement;
