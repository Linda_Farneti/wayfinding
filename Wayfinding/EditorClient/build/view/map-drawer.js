"use strict";
exports.__esModule = true;
var L = require("leaflet");
var array_map_1 = require("../utility/array-map");
var consts_1 = require("../utility/consts");
var list_1 = require("../utility/list");
var pair_impl_1 = require("../utility/pair-impl");
var MapDrawer = /** @class */ (function () {
    function MapDrawer(map, imagePath) {
        this.draggableMap = true;
        this.markerL = L.layerGroup();
        this.nodesL = L.layerGroup();
        this.roadL = L.layerGroup();
        this.pathL = L.layerGroup();
        this.map = map;
        this.mapObserver = new list_1.List();
        this.mapCLC = new array_map_1.ArrayMap();
        this.mapLPL = new array_map_1.ArrayMap();
        this.mapCM = new array_map_1.ArrayMap();
        this.drawMap();
        this.imagePath = imagePath;
    }
    MapDrawer.prototype.onPositionUpdate = function (x, y) {
        if (this.manMarker != undefined && this.manMarker != null) {
            this.map.removeLayer(this.manMarker);
        }
        var iconMan = L.icon({
            iconUrl: this.imagePath + "/marker" + consts_1.Consts.ICON_EXT,
            iconSize: [40, 40]
        });
        this.manMarker = L.marker([x, y]);
        this.manMarker.setIcon(iconMan);
        this.manMarker.addTo(this.map);
    };
    MapDrawer.prototype.onCircleDropped = function (newCircle, event) {
        var _this = this;
        if (!this.draggableMap) {
            this.map.dragging.disable();
            var _a = newCircle.getLatLng(), circleStartingLat_1 = _a.lat, circleStartingLng_1 = _a.lng;
            var _b = event.latlng, mouseStartingLat_1 = _b.lat, mouseStartingLng_1 = _b.lng;
            this.map.on('mousemove', function (event) {
                setTimeout(function () {
                    var _a = event.latlng, mouseNewLat = _a.lat, mouseNewLng = _a.lng;
                    var latDifference = mouseStartingLat_1 - mouseNewLat;
                    var lngDifference = mouseStartingLng_1 - mouseNewLng;
                    newCircle.setLatLng([circleStartingLat_1 - latDifference, circleStartingLng_1 - lngDifference]);
                    var circle = _this.mapCLC.getFirst(newCircle);
                    circle.coordinates = new pair_impl_1.PairImpl(newCircle.getLatLng().lat, newCircle.getLatLng().lng);
                    _this.mapObserver.list().forEach(function (c) {
                        c.onMovingNode(circle);
                        if (_this.mapCM.getSecond(circle) != null) {
                            _this.mapCM.getSecond(circle).setLatLng([circle.coordinates.getX(), circle.coordinates.getY()]);
                        }
                    });
                });
            }, 500);
        }
    };
    MapDrawer.prototype.updateFinalPosition = function (circle) {
        var c = this.mapCLC.getFirst(circle);
        this.mapObserver.list().forEach(function (obs) {
            obs.setFinalNodePosition(c);
        });
    };
    MapDrawer.prototype.addObserver = function (obs) {
        this.mapObserver.add(obs);
    };
    MapDrawer.prototype.drawMap = function () {
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        this.map.addLayer(this.nodesL);
        this.map.addLayer(this.markerL);
        this.map.addLayer(this.roadL);
        this.map.addLayer(this.pathL);
    };
    MapDrawer.prototype.drawCircle = function (c) {
        var _this = this;
        var newCircle = L.circle([c.coordinates.getX(), c.coordinates.getY()], {
            color: c.color,
            fillColor: c.fillColor,
            radius: c.ray,
            fillOpacity: c.opacity
        });
        this.mapCLC.push(c, newCircle);
        newCircle.addTo(this.nodesL);
        newCircle.on("click", function () {
            _this.mapObserver.list().forEach(function (mobs) {
                mobs.onCircleClicked(_this.mapCLC.getFirst(newCircle));
            });
        });
        newCircle.on("mousedown", function (event) {
            _this.onCircleDropped(newCircle, event);
        });
        newCircle.on("mouseup", function () {
            _this.mouseUp(newCircle);
        });
        return c;
    };
    MapDrawer.prototype.mouseUp = function (circle) {
        if (!this.draggableMap) {
            this.updateFinalPosition(circle);
            this.map.dragging.enable();
            this.map.removeEventListener('mousemove');
            this.map.removeEventListener('mousedown');
        }
    };
    MapDrawer.prototype.setDraggableMap = function (c) {
        this.draggableMap = c;
    };
    MapDrawer.prototype.drawLine = function (l) {
        var _this = this;
        var coord = new Array();
        for (var i = 0; i < l.coordinates.length; i++) {
            coord[i] = ([l.coordinates[i].getX(), l.coordinates[i].getY()]);
        }
        var newLine = L.polyline(coord, {
            color: l.color,
            weight: l.weight,
            opacity: l.opacity
        });
        this.mapLPL.push(l, newLine);
        newLine.addTo(this.map);
        newLine.addTo(this.roadL);
        newLine.on("click", function () {
            _this.mapObserver.list().forEach(function (mobs) {
                mobs.onLineClicked(_this.mapLPL.getFirst(newLine));
            });
        });
        // GESTIRE NEL CONTROLLER
        this.showItem(consts_1.Item.NODE, false);
        this.showItem(consts_1.Item.NODE, true);
        return l;
    };
    MapDrawer.prototype.changeStyle = function (e, style) {
        if ('ray' in e) {
            this.mapCLC.getSecond(e).setStyle(style);
        }
        else if ('weight' in e) {
            this.mapLPL.getSecond(e).setStyle(style);
        }
    };
    MapDrawer.prototype.showItem = function (i, c) {
        var layer;
        switch (i) {
            case consts_1.Item.NODE:
                layer = this.nodesL;
                break;
            case consts_1.Item.ROAD:
                layer = this.roadL;
                break;
            default:
                layer = this.markerL;
                break;
        }
        c ? this.map.addLayer(layer) : this.map.removeLayer(layer);
    };
    MapDrawer.prototype.removeLine = function (l) {
        this.roadL.removeLayer(this.mapLPL.getSecond(l));
        for (var i = 0; i < this.mapLPL.entries().size(); i++) {
            if (this.mapLPL.entries().get(i).getX() === l) {
                this.mapLPL.entries().removeIndex(i);
            }
        }
    };
    MapDrawer.prototype.removeCircle = function (c) {
        this.nodesL.removeLayer(this.mapCLC.getSecond(c));
        for (var i = 0; i < this.mapCLC.entries().size(); i++) {
            if (this.mapCLC.entries().get(i).getX() === c) {
                this.mapCLC.entries().removeIndex(i);
            }
        }
        this.removeIcon(c);
    };
    MapDrawer.prototype.addIconToCircle = function (c, path, size) {
        var _this = this;
        var iconMarker = L.icon({
            iconUrl: this.imagePath + "/" + path + consts_1.Consts.ICON_EXT,
            iconSize: [size, size]
        });
        var marker = L.marker([c.coordinates.getX(), c.coordinates.getY()]);
        marker.setIcon(iconMarker);
        this.removeIcon(c);
        marker.addTo(this.markerL);
        this.mapCM.push(c, marker);
        marker.on("click", function () {
            _this.mapObserver.list().forEach(function (mobs) {
                mobs.onCircleClicked(_this.mapCM.getFirst(marker));
                mobs.findDescription(_this.mapCM.getFirst(marker));
            });
        });
        marker.on("mousedown", function (event) {
            _this.onCircleDropped(_this.mapCLC.getSecond(c), event);
        });
        marker.on("mouseup", function () {
            _this.mouseUp(_this.mapCLC.getSecond(c));
            _this.map.dragging.enable();
            _this.map.removeEventListener('mousemove');
        });
    };
    MapDrawer.prototype.resizeCircleIcon = function (c, size) {
        if (this.mapCM.keys().contains(c)) {
            var marker = this.mapCM.getSecond(c);
            var oldIcon = marker.options.icon;
            var newIcon = L.icon({
                iconUrl: oldIcon.options.iconUrl,
                iconSize: [size, size]
            });
            marker.setIcon(newIcon);
        }
    };
    MapDrawer.prototype.removeIcon = function (c) {
        if (this.mapCM.getSecond(c) != null) {
            this.markerL.removeLayer(this.mapCM.getSecond(c));
            this.mapCM.entries().indexOf(new pair_impl_1.PairImpl(c, this.mapCM.getSecond(c)));
        }
    };
    MapDrawer.prototype.getCircleById = function (id) {
        return this.mapCLC.entries().get(id).getX();
    };
    MapDrawer.prototype.selectPath = function (lines) {
        var coord = new Array();
        this.resetPath();
        for (var i = 0; i < lines.size(); i++) {
            coord[i] = ([[lines.get(i).coordinates[0].getX(), lines.get(i).coordinates[0].getY()],
                [lines.get(i).coordinates[1].getX(), lines.get(i).coordinates[1].getY()]]);
        }
        var newLine = L.polyline(coord, {
            color: "green",
            dashArray: '20.15',
            lineJoin: 'round',
            weight: 5,
            opacity: 1
        });
        this.pathList = newLine;
        newLine.addTo(this.map);
        newLine.addTo(this.pathL);
        this.map.addLayer(this.pathL);
    };
    ;
    MapDrawer.prototype.resetPath = function () {
        if (this.pathList != undefined) {
            this.pathL.removeLayer(this.pathList);
        }
    };
    return MapDrawer;
}());
exports.MapDrawer = MapDrawer;
