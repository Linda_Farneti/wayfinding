"use strict";
exports.__esModule = true;
var map_impl_1 = require("../utility/map-impl");
var consts_1 = require("../utility/consts");
var L = require("leaflet");
var MapDrawerImpl = /** @class */ (function () {
    function MapDrawerImpl(map) {
        this.map = map;
        this.mapCLC = new map_impl_1.MapImpl();
        this.mapLPL = new map_impl_1.MapImpl();
        this.drawMap();
        /*   this.defaultStyleN = {
               color: "grey"
           }
           this.highlightStyleN = {
               color: "blue"
           }
   
           this.pathStyleN = {
               fillColor: "green"
           }
   
           this.defaultStyleR = {
               color:"gray", fillColor:"white"
           }
   
           this.pathStyleR = {
               color: "white", fillColor: "blue"
           }*/
    }
    MapDrawerImpl.prototype.drawMap = function () {
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
    };
    MapDrawerImpl.prototype.drawCircle = function (c) {
        var newCircle = L.circle([c.getCoordinates().getX(), c.getCoordinates().getY()], {
            color: c.getColor(),
            fillColor: c.getFillColor(),
            radius: c.getRay(),
            fillOpacity: c.getOpacity()
        });
        this.mapCLC.push(c, newCircle);
        newCircle.addTo(this.map);
        return c;
    };
    MapDrawerImpl.prototype.drawLine = function (l) {
        var newLine = L.polyline([[l.getCoordinates()[0].getX(), l.getCoordinates()[0].getY()], [l.getCoordinates()[1].getX(), l.getCoordinates()[1].getY()]], {
            color: l.getColor(),
            weight: l.getWeight(),
            opacity: l.getOpacity()
        });
        this.mapLPL.push(l, newLine);
        newLine.addTo(this.map);
        return l;
    };
    MapDrawerImpl.prototype.highlightCircle = function (circle) {
        this.mapCLC.getSecond(circle).setStyle(consts_1.Consts.HIGHLIGHT_NODE_STYLE);
    };
    MapDrawerImpl.prototype.defaultCircle = function (circle) {
        this.mapCLC.getSecond(circle).setStyle(consts_1.Consts.DEFAULT_NODE_STYLE);
    };
    return MapDrawerImpl;
}());
exports.MapDrawerImpl = MapDrawerImpl;
