"use strict";
exports.__esModule = true;
var PolygonMaker = /** @class */ (function () {
    function PolygonMaker() {
    }
    PolygonMaker.prototype.convertCoordinates = function (coord) {
        var tempCoord = coord.toString().split(",");
        tempCoord[1] = tempCoord[1].substr(1, tempCoord[1].length - 2);
        tempCoord[0] = tempCoord[0].substr(7, tempCoord[0].length);
        var newCoord = [parseFloat(tempCoord[0]), parseFloat(tempCoord[1])];
        return newCoord;
    };
    PolygonMaker.prototype.generateRoadEndPoints = function (startNode, endNode) {
        var deltaX = endNode[1] - startNode[1];
        var deltaY = endNode[0] - startNode[0];
        var ipotenusa = Math.sqrt(deltaY * deltaY + deltaX * deltaX);
        var angolo = Math.asin(deltaY / ipotenusa);
        var size = 0.00005;
        var signX = 1;
        var signY = 1;
        if (deltaX > 0) {
            signX = -1;
        }
        var p1DeltaX = size * Math.cos(angolo) * signX;
        var p1DeltaY = size * Math.sin(angolo) * signY;
        var newStart = [startNode[0] - p1DeltaY, startNode[1] + p1DeltaX];
        var newEnd = [endNode[0] + p1DeltaY, endNode[1] - p1DeltaX];
        return [newStart, newEnd];
    };
    PolygonMaker.prototype.getCoordinates = function (startNode, endNode) {
        var coord = this.generateRoadEndPoints(this.convertCoordinates(startNode), this.convertCoordinates(endNode));
        var deltaX = coord[1][1] - coord[0][1];
        var deltaY = coord[1][0] - coord[0][0];
        var ipotenusa = Math.sqrt(deltaY * deltaY + deltaX * deltaX);
        var angolo = Math.asin(deltaY / ipotenusa);
        var signX = 1;
        var signY = 1;
        if (deltaX < 0) {
            signY = -1;
        }
        var deltaYK = 0.00003 * Math.cos(angolo) * signY;
        var deltaXK = 0.00003 * Math.sin(angolo) * signX;
        var b1 = [coord[0][0] - deltaYK, coord[0][1] + deltaXK];
        var b2 = [coord[0][0] + deltaYK, coord[0][1] - deltaXK];
        var b4 = [coord[1][0] - deltaYK, coord[1][1] + deltaXK];
        var b3 = [coord[1][0] + deltaYK, coord[1][1] - deltaXK];
        var coordinates = [b1, b2, b3, b4];
        // console.log(coordinates);
        //this.clear();
        return coordinates;
    };
    PolygonMaker.getInstance = function () {
        if (this.pm === null || this.pm === undefined) {
            this.pm = new PolygonMaker();
        }
        return this.pm;
    };
    return PolygonMaker;
}());
exports.PolygonMaker = PolygonMaker;
