"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var consts_1 = require("../utility/consts");
var drawable_element_1 = require("./drawable-element");
var LeafCircle = /** @class */ (function (_super) {
    __extends(LeafCircle, _super);
    function LeafCircle(coord, fC, c, r, o) {
        var _this = _super.call(this, fC, c, o) || this;
        _this.ray = consts_1.Consts.CIRCLE_RAY;
        if (r !== null && r !== undefined) {
            _this.ray = r;
        }
        _this.coordinates = coord;
        return _this;
    }
    LeafCircle.prototype.getRay = function () {
        return this.ray;
    };
    LeafCircle.prototype.getCoordinates = function () {
        return this.coordinates;
    };
    return LeafCircle;
}(drawable_element_1.DrawableElement));
exports.LeafCircle = LeafCircle;
