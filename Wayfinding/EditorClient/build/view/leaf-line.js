"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var consts_1 = require("../utility/consts");
var drawable_element_1 = require("./drawable-element");
var LeafLine = /** @class */ (function (_super) {
    __extends(LeafLine, _super);
    function LeafLine(coord, w, c, fC, o) {
        var _this = _super.call(this, fC, c, o) || this;
        _this.width = consts_1.Consts.ROAD_WIDTH;
        if (w !== null && w !== undefined) {
            _this.width = w;
        }
        _this.coordinates = coord;
        return _this;
    }
    LeafLine.prototype.getCoordinates = function () {
        return this.coordinates;
    };
    LeafLine.prototype.getWeight = function () {
        return this.width;
    };
    return LeafLine;
}(drawable_element_1.DrawableElement));
exports.LeafLine = LeafLine;
