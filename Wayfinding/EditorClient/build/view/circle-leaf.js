"use strict";
exports.__esModule = true;
var CircleLeaf = /** @class */ (function () {
    function CircleLeaf(fC, c, r) {
        this.fillColor = "blue";
        this.ray = 10; // TO DEFINE IN OTHER FILE
        this.color = "cyan";
        if (fC !== null && fC !== undefined) {
            this.fillColor = fC;
        }
        if (c !== null && c !== undefined) {
            this.color = c;
        }
        if (r !== null && r !== undefined) {
            this.ray = r;
        }
    }
    CircleLeaf.prototype.getFillColor = function () {
        return this.fillColor;
    };
    CircleLeaf.prototype.getRay = function () {
        return this.ray;
    };
    CircleLeaf.prototype.getColor = function () {
        return this.color;
    };
    return CircleLeaf;
}());
exports.CircleLeaf = CircleLeaf;
