"use strict";
exports.__esModule = true;
var NodeImpl = /** @class */ (function () {
    function NodeImpl(name, coord) {
        this.name = name;
        this.coord = coord;
    }
    NodeImpl.prototype.getName = function () {
        return this.name;
    };
    NodeImpl.prototype.getCoordinates = function () {
        return this.coord;
    };
    NodeImpl.prototype.getLatitude = function () {
        return this.getCoordinates().getX();
    };
    NodeImpl.prototype.getLongitude = function () {
        return this.getCoordinates().getY();
    };
    NodeImpl.prototype.toString = function () {
        return this.name + " " + this.coord;
    };
    return NodeImpl;
}());
exports.NodeImpl = NodeImpl;
