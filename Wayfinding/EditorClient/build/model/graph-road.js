"use strict";
exports.__esModule = true;
var distance_calculator_1 = require("../utility/distance-calculator");
var pair_impl_1 = require("../utility/pair-impl");
var GraphRoad = /** @class */ (function () {
    function GraphRoad(n, n1, n2, size, map) {
        this.name = n;
        this.startNode = n1;
        this.endNode = n2;
        this.size = size;
        this.distance = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(this.startNode, this.endNode);
        this.map = map;
    }
    GraphRoad.prototype.getName = function () {
        return this.name;
    };
    GraphRoad.prototype.getCoupleNodes = function () {
        var c = new pair_impl_1.PairImpl(this.startNode, this.endNode);
        return c;
    };
    GraphRoad.prototype.getDistance = function () {
        return this.distance;
    };
    GraphRoad.prototype.getSize = function () {
        return this.size;
    };
    GraphRoad.prototype.equals = function (n1, n2) {
        if ((this.startNode === n1 || this.startNode === n2) && (this.endNode === n1 || this.endNode === n2)) {
            return true;
        }
        return false;
    };
    GraphRoad.prototype.toString = function () {
        return "    ID[" + this.name + "]  StartNode[" + this.startNode.getId() + "]   EndNode [" + this.endNode.getId() + "]  Distance: " + this.distance;
    };
    return GraphRoad;
}());
exports.GraphRoad = GraphRoad;
