"use strict";
exports.__esModule = true;
var distance_calculator_1 = require("../utility/distance-calculator");
var pair_impl_1 = require("../utility/pair-impl");
var RoadImpl = /** @class */ (function () {
    function RoadImpl(n, n1, n2) {
        this.name = n;
        this.startNode = n1;
        this.endNode = n2;
        this.distance = distance_calculator_1.DistanceCalculator.getDistance(this.startNode, this.endNode);
    }
    RoadImpl.prototype.getName = function () {
        return this.name;
    };
    RoadImpl.prototype.getCoupleNodes = function () {
        var c = new pair_impl_1.PairImpl(this.startNode, this.endNode);
        return c;
    };
    RoadImpl.prototype.getDistance = function () {
        return this.distance;
    };
    RoadImpl.prototype.equals = function (n1, n2) {
        if ((this.startNode === n1 || this.startNode === n2) && (this.endNode === n1 || this.endNode === n2)) {
            return true;
        }
        return false;
    };
    RoadImpl.prototype.toString = function () {
        return "Road " + this.name + "\n\tNode" + this.startNode.toString() + "\n\tNode " + this.endNode.toString() + "\Distance: " + this.distance;
    };
    return RoadImpl;
}());
exports.RoadImpl = RoadImpl;
