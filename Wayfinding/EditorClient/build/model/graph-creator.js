"use strict";
exports.__esModule = true;
var distance_calculator_1 = require("../utility/distance-calculator");
var list_1 = require("../utility/list");
var pair_impl_1 = require("../utility/pair-impl");
var graph_node_1 = require("./graph-node");
var graph_road_1 = require("./graph-road");
var GraphCreator = /** @class */ (function () {
    function GraphCreator(mapId) {
        this.nodes = new list_1.List();
        this.roads = new list_1.List();
        this.points = new list_1.List();
        this.nodeSelect = null;
        this.mapId = mapId;
    }
    GraphCreator.prototype.getNearest = function (map, t) {
        var n;
        var distance = Number.POSITIVE_INFINITY;
        t.list().forEach(function (p) {
            if (map.getSecond(p) < distance) {
                n = p;
                distance = map.getSecond(p);
            }
        });
        return n;
    };
    GraphCreator.prototype.getNeighbor = function (node) {
        var neighbors = new list_1.List();
        this.getRoadsFromNode(node).list().forEach(function (r) {
            neighbors.add(r.getCoupleNodes().getX() === node ? r.getCoupleNodes().getY() : r.getCoupleNodes().getX());
        });
        return neighbors;
    };
    GraphCreator.prototype.compactForJSON = function () {
        var _this = this;
        var _loop_1 = function (i) {
            if (this_1.nodes.get(i).getId() !== i) {
                this_1.points.list().forEach(function (p) {
                    if (p.id === _this.nodes.get(i).getId()) {
                        p.id = i;
                    }
                });
                this_1.nodes.get(i).setId(i);
            }
        };
        var this_1 = this;
        for (var i = 0; i < this.nodes.size(); i++) {
            _loop_1(i);
        }
    };
    GraphCreator.prototype.getPointByNodeId = function (id) {
        var point = undefined;
        this.points.list().forEach(function (p) {
            if (p.nodeID === id) {
                point = p;
            }
        });
        return point;
    };
    GraphCreator.prototype.deleteMap = function () {
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Map/DeleteMap?ID=" + this.mapId,
            contentType: "application/json",
            // data: JSON.stringify({ ID: this.mapId }),
            success: function () {
                window.location.href = "http://localhost/Wayfinding/GetStarted";
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.getRoadBetweenNodes = function (n1, n2) {
        var road;
        this.getRoadsFromNode(n1).list().forEach(function (r) {
            if (r.getCoupleNodes().getX() === n2 || r.getCoupleNodes().getY() === n2) {
                road = r;
            }
        });
        return road;
    };
    GraphCreator.prototype.roadExist = function (n1, n2) {
        var r = this.getRoadBetweenNodes(n1, n2);
        if (r != null && r != undefined) {
            return true;
        }
        else {
            return false;
        }
    };
    GraphCreator.prototype.addNode = function (coord, callback) {
        var _this = this;
        var n;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Node/AddNode",
            contentType: "application/json",
            data: JSON.stringify({ Latitude: coord.getX(), Longitude: coord.getY(), Map: this.mapId }),
            success: function (i) {
                n = new graph_node_1.GraphNode(Number(i.ID), coord);
                _this.nodes.add(n);
                callback(n);
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.addIconToPoint = function (name, nodeID, size, description, path, callback) {
        var _this = this;
        var newPoint;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Point/AddPoint",
            contentType: "application/json",
            data: JSON.stringify({ Name: name, NodeID: nodeID, Size: size, Description: description, Icon: path, Map: this.mapId }),
            complete: function (i) {
                var id = Number(i.responseText);
                newPoint = { id: id, name: name, nodeID: nodeID, size: size, description: description, path: path };
                _this.points.add(newPoint);
                callback();
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.positionAvailableForNodes = function (coord) {
        var newNode = new graph_node_1.GraphNode(this.nodes.size() + 1, coord);
        for (var i = 0; i < this.nodes.list().length; i++) {
            if (distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(this.nodes.get(i), newNode) < 20) {
                return false;
            }
        }
        return true;
    };
    GraphCreator.prototype.getSelectedNode = function () {
        return this.nodeSelect;
    };
    GraphCreator.prototype.getSelectedRoad = function () {
        return this.roadSelect;
    };
    GraphCreator.prototype.addRoad = function (n1, n2, size, callback) {
        var _this = this;
        var r;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Road/AddRoad",
            contentType: "application/json",
            data: JSON.stringify({ Length: distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(n1, n2), StartNodeID: n1.getId(), EndNodeID: n2.getId(), Size: size, Map: this.mapId }),
            complete: function (i) {
                r = new graph_road_1.GraphRoad(Number(i.responseText), n1, n2, size, _this.mapId);
                // console.log("Road adde in model");
                _this.roads.add(r);
                callback(r);
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.getRoads = function () {
        return this.roads;
    };
    GraphCreator.prototype.getRoadsFromNode = function (node) {
        var roadVet = new list_1.List();
        this.roads.list().forEach(function (r) {
            if (r.getCoupleNodes().getX() === node || r.getCoupleNodes().getY() === node) {
                roadVet.add(r);
            }
        });
        return roadVet;
    };
    GraphCreator.prototype.removeRoad = function (r, callback) {
        var _this = this;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Road/RemoveRoad?ID=" + r.getName(),
            contentType: "application/json",
            //  data: JSON.stringify({ ID: r.getName() }),
            complete: function () {
                _this.roads.remove(r);
                callback();
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.removeNode = function (n) {
        var _this = this;
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Node/RemoveNode?ID=" + n.getId(),
            contentType: "application/json",
            //   data: JSON.stringify({ ID: n.getId() }),
            complete: function () {
                _this.nodes.removeIndex(_this.nodes.indexOf(n));
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.removePoint = function (n) {
        var _this = this;
        var num;
        var pointToRemove = this.getPointByNodeId(n);
        if (pointToRemove !== undefined) {
            $.ajax({
                type: "POST",
                url: "http://localhost/Wayfinding/Api/Point/RemovePoint?ID=" + n,
                contentType: "application/json",
                // data: JSON.stringify({ ID: n }),
                complete: function () {
                    _this.points.remove(pointToRemove);
                },
                dataType: "json"
            });
        }
    };
    GraphCreator.prototype.getPointDescription = function (nodeId) {
        var desc = "";
        this.points.list().forEach(function (p) {
            if (p.nodeID == nodeId) {
                desc = p.description;
            }
        });
        return desc;
    };
    GraphCreator.prototype.getNodeById = function (id) {
        var node;
        this.nodes.list().forEach(function (n) {
            if (n.getId() === id) {
                node = n;
            }
        });
        return node;
    };
    GraphCreator.prototype.setJSON = function () {
        this.compactForJSON();
        return JSON.stringify(new Array(this.nodes, this.roads, this.points));
    };
    GraphCreator.prototype.onDescriptionSet = function (nodeID, text) {
        var point = this.points.list().forEach(function (p) {
            if (p.nodeID == nodeID) {
                $.ajax({
                    type: "POST",
                    url: "http://localhost/Wayfinding/Api/Point/UpdatePoint",
                    contentType: "application/json",
                    data: JSON.stringify({ ID: nodeID, Size: null, Desc: text }),
                    complete: function () {
                        p.description = text;
                    },
                    dataType: "json"
                });
            }
        });
    };
    ;
    GraphCreator.prototype.getPath = function (startNode, endNode, callback) {
        var a = startNode;
        var b = endNode;
        var roadVet = new list_1.List();
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Road/GetPath?FirstNode=" + startNode.getId() + "&SecondNode=" + endNode.getId() + "&MapID=" + this.mapId,
            contentType: "application/json",
            //data: JSON.stringify({ FirstNode: startNode, SecondNode:endNode, MapID: this.mapId }),
            success: function (i) {
                var x = new Array(i);
                for (var a_1 = 0; a_1 < x[0].length; a_1++) {
                    roadVet.add(x[0][a_1].Id);
                }
                callback(roadVet);
            },
            dataType: "json"
        });
        return null;
    };
    /*public getPath(startNode: Node, endNode: Node): IList<Road> {

        let s: IList<Node> = new List<Node>();
        let t: IList<Node> = new List<Node>();
        let f: Map<Node, number> = new ArrayMap<Node, number>();
        let j: Map<Node, Node> = new ArrayMap<Node, Node>();

        let neighbors: IList<Node> = new List<Node>();
        let distance: number;
        let nearest: Node;

        this.nodes.list().forEach(node => {
            f.push(node, Number.POSITIVE_INFINITY);
            j.push(node, undefined);
        });

        f.replace(startNode, startNode, 0);
        this.getNeighbor(startNode).list().forEach(n => {
            f.replace(n, n, this.getRoadBetweenNodes(startNode, n).getDistance());
        });
        t.addAll(this.nodes.list());
        t.remove(startNode);

        while (!t.isEmpty()) {
            nearest = this.getNearest(f, t);
            if (this.getNeighbor(startNode).contains(nearest)) {
                j.replace(nearest, nearest, startNode);
            }
            t.remove(nearest);
            s.add(nearest);
            neighbors = this.getNeighbor(nearest);
            neighbors.removeAll(s.list());
            neighbors.list().forEach(n => {
                distance = f.getSecond(nearest) + this.getRoadBetweenNodes(nearest, n).getDistance();
                if (distance < f.getSecond(n)) {
                    f.replace(n, n, distance);
                    j.replace(n, n, nearest);
                }
            });
        }
        let path: IList<Road> = new List<Road>();
        let next = endNode;
        let prev;
        do {
            prev = j.getSecond(next);
            path.add(this.getRoadBetweenNodes(next, prev));
            next = prev;
        } while (next != startNode);

        return path;
    }*/
    GraphCreator.prototype.resizePointIcon = function (id, size) {
        var point = this.getPointByNodeId(id);
        var old;
        if (point !== undefined) {
            $.ajax({
                type: "POST",
                url: "http://localhost/Wayfinding/Api/Point/UpdatePoint",
                contentType: "application/json",
                data: JSON.stringify({ ID: id, Size: size, Desc: null }),
                complete: function () {
                    old = point.size;
                    point.size = size;
                    //                 console.log("Resize icon of node[ "+id+" ] from size "+old+" to size "+ size);
                },
                dataType: "json"
            });
        }
    };
    GraphCreator.prototype.updateNodePosition = function (nodeId, latitude, longitude) {
        var node = this.getNodeById(nodeId);
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Node/UpdateNodePosition",
            contentType: "application/json",
            data: JSON.stringify({ ID: nodeId, Latitude: parseFloat(latitude.toString()), Longitude: parseFloat(longitude.toString()) }),
            dataType: "json"
        });
    };
    GraphCreator.prototype.updateRoad = function (r, newLength, newSize) {
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Road/UpdateRoad",
            contentType: "application/json",
            data: JSON.stringify({ ID: r.getName(), NewLength: newLength, NewSize: newSize }),
            dataType: "json"
        });
    };
    GraphCreator.prototype.loadNodes = function (callback) {
        var _this = this;
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Node/GetNodes?MapID=" + this.mapId,
            contentType: "application/json",
            success: function (d) {
                var a = new Array(d);
                for (var i = 0; i < a[0].length; i++) {
                    _this.nodes.add(new graph_node_1.GraphNode(a[0][i].Id, new pair_impl_1.PairImpl(a[0][i].Latitude, a[0][i].Longitude)));
                }
                callback(_this.nodes);
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.loadRoads = function (callback) {
        var _this = this;
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Road/GetRoads?MapID=" + this.mapId,
            contentType: "application/json",
            success: function (d) {
                var a = new Array(d);
                var c_roads = new list_1.List();
                var _loop_2 = function (i) {
                    var StartNode, EndNode;
                    _this.nodes.list().forEach(function (n) {
                        if (n.getId() == a[0][i].StartNode) {
                            StartNode = n;
                        }
                        else if (n.getId() == a[0][i].EndNode) {
                            EndNode = n;
                        }
                    });
                    c_roads.add(new graph_road_1.GraphRoad(a[0][i].Id, StartNode, EndNode, a[0][i].Size, a[0][i].Map));
                    _this.roads.add(new graph_road_1.GraphRoad(a[0][i].Id, StartNode, EndNode, a[0][i].Size, a[0][i].Map));
                };
                for (var i = 0; i < a[0].length; i++) {
                    _loop_2(i);
                }
                callback(c_roads);
            },
            dataType: "json"
        });
    };
    GraphCreator.prototype.loadPois = function (callback) {
        var _this = this;
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Point/GetPoints?MapID=" + this.mapId,
            contentType: "application/json",
            success: function (d) {
                var a = new Array(d);
                var newPoint, id, name, nodeID, size, description, path;
                for (var i = 0; i < a[0].length; i++) {
                    id = a[0][i].Id;
                    name = a[0][i].Name;
                    nodeID = a[0][i].NodeID;
                    size = a[0][i].Size;
                    description = a[0][i].Description;
                    path = a[0][i].Icon,
                        newPoint = { id: id, name: name, nodeID: nodeID, size: size, description: description, path: path };
                    _this.points.add(newPoint);
                }
                callback(_this.points);
            },
            dataType: "json"
        });
    };
    return GraphCreator;
}());
exports.GraphCreator = GraphCreator;
