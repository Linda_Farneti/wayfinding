"use strict";
exports.__esModule = true;
var GraphNode = /** @class */ (function () {
    function GraphNode(id, coord) {
        this.id = id;
        this.coord = coord;
    }
    GraphNode.prototype.getId = function () {
        return this.id;
    };
    GraphNode.prototype.setId = function (id) {
        this.id = id;
    };
    GraphNode.prototype.getCoordinates = function () {
        return this.coord;
    };
    GraphNode.prototype.setCoordinates = function (p) {
        this.coord = p;
    };
    GraphNode.prototype.getLatitude = function () {
        return this.getCoordinates().getX();
    };
    GraphNode.prototype.getLongitude = function () {
        return this.getCoordinates().getY();
    };
    GraphNode.prototype.toString = function () {
        return "ID = [" + this.id + "]" + "Coord : [" + this.coord.getX() + "," + this.coord.getY() + "]";
    };
    return GraphNode;
}());
exports.GraphNode = GraphNode;
