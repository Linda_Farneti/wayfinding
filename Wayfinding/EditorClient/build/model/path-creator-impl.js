"use strict";
exports.__esModule = true;
var PathCreator = /** @class */ (function () {
    function PathCreator(sn, en) {
        this.startNode = sn;
        this.endNode = en;
    }
    return PathCreator;
}());
