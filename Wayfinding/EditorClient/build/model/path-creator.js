"use strict";
exports.__esModule = true;
var PathCreator = /** @class */ (function () {
    function PathCreator() {
    }
    PathCreator.getPath = function (startNode, endNode, roads) {
        console.log("Calculating path from node ", startNode, " to node ", endNode);
        console.log("Roads: ", roads.toString());
    };
    return PathCreator;
}());
exports.PathCreator = PathCreator;
