"use strict";
exports.__esModule = true;
var node_impl_1 = require("./node-impl");
var distance_calculator_1 = require("../utility/distance-calculator");
var road_impl_1 = require("./road-impl");
var GraphImpl = /** @class */ (function () {
    function GraphImpl() {
        this.nodes = new Array();
        this.roads = new Array();
        this.nodeSelect = null;
    }
    GraphImpl.prototype.roadExist = function (n1, n2) {
        var c = false;
        this.roads.forEach(function (r) {
            if (r.equals(n1, n2)) {
                c = true;
            }
        });
        return c;
    };
    GraphImpl.prototype.addNode = function (index, coord) {
        var n = new node_impl_1.NodeImpl(index, coord);
        this.nodes.push(n);
        return n;
    };
    GraphImpl.prototype.positionAvailable = function (coord) {
        var posAvailable = true;
        var newNode = new node_impl_1.NodeImpl(this.nodes.length + 1, coord);
        for (var i = 0; i < this.nodes.length && posAvailable === true; i++) {
            if (distance_calculator_1.DistanceCalculator.getDistance(this.nodes[i], newNode) < 20) {
                posAvailable = false;
                this.nodeSelect = this.nodes[i];
            }
        }
        return posAvailable;
    };
    GraphImpl.prototype.getSelectedNode = function () {
        return this.nodeSelect;
    };
    GraphImpl.prototype.addRoad = function (index, n1, n2) {
        var r = new road_impl_1.RoadImpl(index, n1, n2);
        if (!this.roadExist(r.getCoupleNodes().getX(), r.getCoupleNodes().getY())) {
            this.roads.push(r);
        }
        return r;
    };
    GraphImpl.prototype.getNodeIndex = function () {
        return this.nodes.length + 1;
    };
    GraphImpl.prototype.getRoadIndex = function () {
        return this.roads.length + 1;
    };
    GraphImpl.prototype.getRoads = function () {
        return this.roads;
    };
    return GraphImpl;
}());
exports.GraphImpl = GraphImpl;
