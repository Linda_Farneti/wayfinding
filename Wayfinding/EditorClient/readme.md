TODO:
(DONE) - POJO: utilizzare le interfacce per gli oggetti di tipo pojo da creare e passare quelle interfacce nei metodi; 
(DONE) - arrow function nelle callback;
(DONE) - costruttori con parametri ridotti;
(DONE) - polyline con selezione di più nodi;
(DONE) - inserimento immagini svg;
(DONE) - eliminazione nodi e strade collegate
(DONE) - eliminazione strade singole    
(DONE) - spostamento nodi e relative strade al click 
(DONE) - esportazione e importazione mappa disegnata tramite json
(DONE) - non fare trascinare i nodi se non si è in stato MOVE
(DONE) - possibilità di scegliere la dimensione delle icone
(DONE) - memorizzare il JSON in un file di testo   
(DONE) - gestire il fatto di poter decidere prima della creazione la larghezza di una nuova strada
(DONE) - strada più piccola che viene disegnata per (sopra) la visualizzazione del percorso minimo
(DONE) -  memorizzare il concetto di icona (point of interest) nel model (point of interest - icon - marker);
(ALMOST DONE) - memorizzare le tipologie di icona in un JSON che viene importato nel setup e fare in modo che all'apertura della pagina                    vengano disegnate le tipologie di icone. Quando il documento viene esportato salvare solo gli ID per le tipologie di icone.

- simulare il movimento di un omino nella mappa dato un insieme di coordinate che definiscono un percorso 
- visualizzare l'attività che si sta svolgendo (Es. creazione dei nodi)

(?) - evitare che i nodi, dopo il trascinamento, si sovrappongano agli altri 


SVILUPPI FUTURI:
-   cambiare lo stile di un'icona selezionata
-   gestione spessore oggetti in base allo zoom;
