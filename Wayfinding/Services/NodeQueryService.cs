﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;

namespace Wayfinding.Services
{
    public class NodeQueryService : INodeQS
    {
        public Array GetNodes(int MapID)
        {
            var database = new WayFindingUniBOEntities1();
            var nodes = database.Node.Where(x=> x.Map == MapID);

            var nodesDtos = nodes.Select(
                 n => new Models.NodeDto
                 {
                     Id = n.ID,
                     Latitude = (float)n.Latitude,
                     Longitude = (float)n.Longitude
                 }).ToList();
            var nodesList = new Models.NodesListDto
            {
                Nodes = nodesDtos.ToArray()
            };
            return nodesList.Nodes;
        }

        public List<int> GetNeighbours(int ID, int MapID)
        {
            List<int> neighbours = new List<int>();
            List<Road> roads = new List<Road>();
            List<int> neighboursIds = new List<int>();
            var database = new WayFindingUniBOEntities1();
            roads = database.Road.Where(r => (r.StartNodeID == ID || r.EndNodeID == ID) && r.Map1.ID == MapID).ToList();
            roads.ForEach(r =>
            {
                int first = r.StartNodeID;
                int second = r.EndNodeID;
                if (first != ID)
                {
                    neighbours.Add(first);
                }
                if (second != ID)
                {
                    neighbours.Add(second);
                }
            });
            return neighbours;
        }

        public Node GetNearest(Dictionary<int, double> map, List<Node> list)
        {
            Node n = new Node();
            double distance = (double)int.MaxValue;

            list.ForEach(node =>
            {
                double d = map[node.ID];
                if ((double)map[node.ID] < distance)
                {
                    n = node;
                    distance = (double)map[node.ID];
                }
            });
            return n;
        }
    }
}