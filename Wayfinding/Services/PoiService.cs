﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;
using Wayfinding.Models;
using Wayfinding.Services;

namespace Wayfinding.Services
{
    public class PoiService : IPointS
    {
        public int AddPoint(ViewModels.AddPoint vm)
        {
            var database = new WayFindingUniBOEntities1();
            var point = new PointOfInterest
            {
                Name = vm.Name,
                NodeID = vm.NodeID,
                Size = vm.Size,
                Description = vm.Description,
                Icon = vm.Icon,
                Map = vm.Map
            };
            database.PointOfInterest.Add(point);
            database.SaveChanges();
            return point.ID;
        }

        public int RemovePoint(int ID)
        {
            var database = new WayFindingUniBOEntities1();
            PointOfInterest toRemove = database.PointOfInterest.First(n => n.NodeID == ID);
            database.Entry(toRemove).State = System.Data.Entity.EntityState.Deleted;
            database.SaveChanges();
            return toRemove.ID;
        }

        public int UpdatePointSize(int ID, int Size)
        {
            var database = new WayFindingUniBOEntities1();
            PointOfInterest toUpdate = database.PointOfInterest.First(n => n.NodeID == ID);
            toUpdate.Size = Size;
            database.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
            database.SaveChanges();
            return Size;
        }

        public string UpdatePointDescription(int ID, String desc)
        {
            var database = new WayFindingUniBOEntities1();
            PointOfInterest toUpdate = database.PointOfInterest.First(n => n.NodeID == ID);
            toUpdate.Description = desc;
            database.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
            database.SaveChanges();
            return desc;
        }
    }
}