﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;
using Wayfinding.ViewModels;

namespace Wayfinding.Services
{
    public class RoadService : IRoadS
    {
        public int AddRoad(AddRoad vm)
        {
            var database = new WayFindingUniBOEntities1();
            var road = new Road
            {
                Length = vm.Length,
                StartNodeID = vm.StartNodeID,
                EndNodeID = vm.EndNodeID,
                RoadSize = vm.Size,
                Map = vm.Map
            };
            database.Road.Add(road);
            database.SaveChanges();
            return road.ID;
        }

        public int RemoveRoad(int ID)
        {
            var database = new WayFindingUniBOEntities1();
            Road toRemove = database.Road.Find(ID);
            database.Entry(toRemove).State = System.Data.Entity.EntityState.Deleted;
            database.SaveChanges();
            return toRemove.ID;
        }

        public int UpdateRoad(UpdateRoad vm)
        {
            var database = new WayFindingUniBOEntities1();
            Road toUpdate = database.Road.Find(vm.ID);
            if (vm.NewLength != null)
            {
                toUpdate.Length = (float)vm.NewLength;
            }
            else
            {
                toUpdate.RoadSize = (int)vm.NewSize;
            }
            database.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
            database.SaveChanges();
            return vm.ID;
        }
    }
}