﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wayfinding.Services
{
    interface INodeQS
    {
        Array GetNodes(int MapID);
        List<int> GetNeighbours(int ID, int MapID);
        Node GetNearest(Dictionary<int, double> map, List<Node> list);
    }
}
