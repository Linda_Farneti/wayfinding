﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;

namespace Wayfinding.Services
{
    public class RoadQueryService : IRoadQS
    {
        private NodeQueryService nqs = new NodeQueryService();

        public Array GetRoads(int MapID)
        {
            var database = new WayFindingUniBOEntities1();
            var roads = database.Road.Where(x => x.Map == MapID);
            var roadsDto = roads.Select(
                r => new Models.RoadDto
                {
                    Id = r.ID,
                    Length = r.Length,
                    StartNode = r.StartNodeID,
                    EndNode = r.EndNodeID,
                    Size = r.RoadSize
                }).ToList();

            var roadList = new Models.RoadsListDto
            {
                Roads = roadsDto.ToArray()
            };
            return roadList.Roads;
        }

        //localhost/Wayfinding/Api/Road/GetPath?FirstNode= &SecondNode= &MapID= 
        public Array GetPath(int FirstNode, int SecondNode, int MapID)
        {
            var database = new WayFindingUniBOEntities1();
            int a = FirstNode;
            int b = SecondNode;
            List<Road> path = new List<Road>();
            List<int> s = new List<int>();
            List<Node> t = new List<Node>();
            List<int> neighbours = new List<int>();
            Dictionary<int, double> f = new Dictionary<int, double>();
            Dictionary<int, int?> j = new Dictionary<int, int?>();
            double distance;
            Node StartNode = database.Node.Single(n => n.ID == FirstNode);
            Node EndNode = database.Node.Single(n => n.ID == SecondNode);
            Node nearest;
            Node Next;
            int? Prev;

            database.Node.Where(x=> x.Map1.ID == MapID).ToList().ForEach(n =>
            {
                f.Add(n.ID, (double)int.MaxValue);
                j.Add(n.ID, null);
            });

            Console.Write(f);
            Console.Write(j);

            f.Remove(StartNode.ID);
            f.Add(StartNode.ID, 0);
            this.nqs.GetNeighbours(FirstNode, MapID).ForEach(n =>
            {
                f.Remove(n);
                f.Add(n, this.GetRoadBetweenNodes(FirstNode, n).Length);
            });
            t = database.Node.Where(x => x.Map1.ID == MapID).ToList();
            t.Remove(StartNode);

            while (t.Count > 0)
            {
                nearest = this.nqs.GetNearest(f, t);
                if (this.nqs.GetNeighbours(StartNode.ID, MapID).Contains(nearest.ID))
                {
                    j.Remove(nearest.ID);
                    j.Add(nearest.ID, StartNode.ID);
                }
                t.Remove(nearest);
                s.Add(nearest.ID);
                neighbours = this.nqs.GetNeighbours(nearest.ID, MapID);
                s.ForEach(x => neighbours.Remove(x));
                neighbours.ForEach(n =>
                {
                    distance = f[nearest.ID] + this.GetRoadBetweenNodes(nearest.ID, n).Length;
                    if (distance < f[n])
                    {
                        f.Remove(n);
                        f.Add(n, distance);
                        j.Remove(n);
                        j.Add(n, nearest.ID);
                    }
                });
            }
            Next = database.Node.Find(SecondNode);
            do
            {
                Prev = j[Next.ID];
                path.Add(this.GetRoadBetweenNodes(Next.ID, Prev));
                Next = database.Node.Find(Prev);
            } while (Next != StartNode);

            var roadsDto = path.Select(
                r => new Models.RoadDto
                {
                    Id = r.ID,
                    Length = r.Length,
                    StartNode = r.StartNodeID,
                    EndNode = r.EndNodeID,
                    Size = r.RoadSize
                }).ToList();

            var roadList = new Models.RoadsListDto
            {
                Roads = roadsDto.ToArray()
            };
            return roadList.Roads;
        }

        public Road GetRoadBetweenNodes(int? FirstNodeID, int? SecondNodeID)
        {
            var database = new WayFindingUniBOEntities1();
            Road toReturn = database.Road.Where(r => (r.StartNodeID == FirstNodeID && r.EndNodeID == SecondNodeID) ||
                                                     (r.StartNodeID == SecondNodeID && r.EndNodeID == FirstNodeID)).Single();
            return toReturn;
        }
    }
}