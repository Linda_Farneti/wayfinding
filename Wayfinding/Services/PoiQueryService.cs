﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;

namespace Wayfinding.Services
{
    public class PoiQueryService : IPointQS
    {
        public Array GetPoints(int MapID)
        {
            var database = new WayFindingUniBOEntities1();
            var points = database.PointOfInterest.Where(x=> x.Map == MapID);
            var pointsDtos = points.Select(
                 p => new Models.POIDto
                 {
                     Id = p.ID,
                     Name = p.Name,
                     NodeID = p.NodeID,
                     Size = p.Size,
                     Description = p.Description,
                     Icon = p.Icon
                 }).ToList();
            var pointsList = new Models.POIsListDto
            {
                POIs = pointsDtos.ToArray()
            };
            return pointsList.POIs;
        }
    }
}