﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wayfinding.Services
{
    interface IPointQS
    {
        Array GetPoints(int MapID);
    }
}
