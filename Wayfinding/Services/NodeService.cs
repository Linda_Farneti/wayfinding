﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;
using Wayfinding.ViewModels;
using Wayfinding.ViewModels.Node;

namespace Wayfinding.Services
{
    public class NodeService : INodeS
    {
        public Node AddNode(AddNode vm)
        {
            var database = new WayFindingUniBOEntities1();
            var node = new Node
            {
                Latitude = vm.Latitude,
                Longitude = vm.Longitude,
                Map = vm.Map,
            };
            database.Node.Add(node);
            database.SaveChanges();
            return node;
        }

        public int RemoveNode(int ID)
        {
            var database = new WayFindingUniBOEntities1();
            Node toRemove = database.Node.Find(ID);
            database.Entry(toRemove).State = System.Data.Entity.EntityState.Deleted;
            database.SaveChanges();
            return toRemove.ID;
        }

        public int UpdateNodePosition(UpdateNode vm)
        {
            var database = new WayFindingUniBOEntities1();
            Node toUpdate = database.Node.Find(vm.ID);
            toUpdate.Latitude = vm.Latitude;
            toUpdate.Longitude = vm.Longitude;
            database.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
            database.SaveChanges();
            return toUpdate.ID;
        }
    }
}