﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wayfinding.ViewModels;

namespace Wayfinding.Services
{
    interface INodeS
    {
        Node AddNode(AddNode vm);
        int RemoveNode(int ID);
        int UpdateNodePosition(UpdateNode vm);
    }
}
