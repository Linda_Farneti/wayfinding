﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wayfinding.ViewModels;

namespace Wayfinding.Services
{
    interface IRoadS
    {
        int AddRoad(AddRoad vm);
        int RemoveRoad(int ID);
        int UpdateRoad(UpdateRoad vm);
    }
}
