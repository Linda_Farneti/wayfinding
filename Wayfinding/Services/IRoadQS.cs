﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wayfinding.Services
{
    interface IRoadQS
    {
        Array GetRoads(int MapID);
        Array GetPath(int FirstNode, int SecondNode, int MapID);
        Road GetRoadBetweenNodes(int? FirstNodeID, int? SecondNodeID);
    }
}
