﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wayfinding.Services
{
    interface IPointS
    {
        int AddPoint(ViewModels.AddPoint vm);
        int RemovePoint(int ID);
        int UpdatePointSize(int ID, int Size);
        string UpdatePointDescription(int ID, String desc);
    }
}
