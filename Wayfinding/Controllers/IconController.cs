﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using EntityModels;
using Newtonsoft.Json;

namespace Wayfinding.Controllers
{
    public class IconController : Controller
    {
        [HttpPost]
        public ActionResult StoreIcon(HttpPostedFileBase FileUpload)
        {
            var database = new WayFindingUniBOEntities1();
            var icon = new EntityModels.Icon
            {
                Name = FileUpload.FileName.ToString().Replace(".svg", "")
            };

            database.Icon.Add(icon);
            database.SaveChanges();
            if (FileUpload != null && FileUpload.ContentLength > 0)
            {
                var fileName = Path.GetFileName(FileUpload.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                FileUpload.SaveAs(path);
            }

            return RedirectToAction("Index", "GetStarted", new { uploaded = 1});
        }

    }
}