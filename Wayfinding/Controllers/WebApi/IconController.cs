﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using EntityModels;

namespace Wayfinding.Controllers.WebApi
{
    public class IconController : ApiController
    {

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetIcons()
        {
            var database = new WayFindingUniBOEntities1();
            var icons = database.Icon.ToList();
            var iconDtos = icons.Select(
                i => new Models.IconDto
                {
                    Id = i.ID,
                    Name = i.Name,
                }
            );
            return Ok(iconDtos.ToArray());

        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult RemoveIcon(int ID)
        {
            var database = new WayFindingUniBOEntities1();
            Icon toRemove = database.Icon.Find(ID);
            database.Entry(toRemove).State = System.Data.Entity.EntityState.Deleted;
            database.SaveChanges();
            return Ok();
        }
    }
}
