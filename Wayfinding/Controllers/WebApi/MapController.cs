﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using EntityModels;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class MapController : ApiController
    {
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddMap(AddMap vm)
        {
            var database = new WayFindingUniBOEntities1();
            var map = new Map
            {
                Name = vm.Name,
                Latitude = vm.Latitude,
                Longitude = vm.Longitude,
                Zoom = vm.Zoom,
                Username = vm.Username
            };

            database.Map.Add(map);
            database.SaveChanges();
            Map created = database.Map.Where(x => x.Name == vm.Name).First();
            return Ok(created.ID);
        }
        
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetMapFromID(int ID)
        {
            var database = new WayFindingUniBOEntities1();
            var toReturn = database.Map.Find(ID);

            var mapToReturn = new Map
            {
                ID = toReturn.ID,
                Name = toReturn.Name,
                Latitude = toReturn.Latitude,
                Longitude = toReturn.Longitude,
                Zoom = toReturn.Zoom,
            };

            return Ok(mapToReturn);
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetMaps(string User)
        {
            var database = new WayFindingUniBOEntities1();
            var maps = database.Map.Where(x=> x.Username.Equals(User)).ToList();
            var mapDtos = maps.Select(
                m => new Models.MapDto
                {
                    Id = m.ID,
                    Name = m.Name,
                    Latitude = (float)m.Latitude,
                    Longitude = (float)m.Longitude,
                    Zoom = m.Zoom,
                }
                );
            var mapList = new Models.MapListDto
            {
                Maps = mapDtos.ToArray()
            };
            return Ok(mapList.Maps);
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult DeleteMap(int ID)
        {
            var database = new WayFindingUniBOEntities1();
            NodeController nc = new NodeController();
            RoadController rc = new RoadController();
            PointController pc = new PointController();
            int a = ID;
            List<int> nodes = database.Node.Where(y => y.Map == ID).Select(x => x.ID).ToList();
            List<int> pois = database.PointOfInterest.Where(y => y.Map == ID).Select(x => x.NodeID).ToList();
            List<int> roads = database.Road.Where(y => y.Map == ID).Select(x => x.ID).ToList();

            foreach(int p in pois)
            {
                pc.RemovePoint(p);
            }
            foreach (int r in roads)
            {
                rc.RemoveRoad(r);
            }
            foreach (int n in nodes)
            {
                nc.RemoveNode(n);
            }
            Map toRemove = database.Map.Find(ID);
            database.Entry(toRemove).State = System.Data.Entity.EntityState.Deleted;
            database.SaveChanges();
            return Ok(ID);
        }
    }
}
