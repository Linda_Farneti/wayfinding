﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Wayfinding.ViewModels;
using Wayfinding.ViewModels.Node;

namespace Wayfinding.Controllers.WebApi
{
    public interface IDbNodeController
    {
        IHttpActionResult GetNodes(int MapID);
        IHttpActionResult AddNode(AddNode ian);
        IHttpActionResult RemoveNode(int ID);
        IHttpActionResult UpdateNodePosition(UpdateNode iun);

    }
}
