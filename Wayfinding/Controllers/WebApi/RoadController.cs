﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wayfinding.Services;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class RoadController : ApiController, IDbRoadController
    {
        private RoadQueryService rqs;
        private RoadService rs;

        public RoadController()
        {
            this.rqs = new RoadQueryService();
            this.rs = new RoadService();
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetRoads(int MapID)
        {
            return Ok(this.rqs.GetRoads(MapID));
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetPath(int FirstNode, int SecondNode, int MapID)
        {
            return Ok(this.rqs.GetPath(FirstNode, SecondNode, MapID));
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult AddRoad(AddRoad vm)
        {
            return Json(this.rs.AddRoad(vm));
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult RemoveRoad(int ID)
        {
            return Ok(this.rs.RemoveRoad(ID));
        }

       
        public IHttpActionResult UpdateRoad(UpdateRoad vm)
        {
            return Ok(this.rs.UpdateRoad(vm));
        }
    }
}

