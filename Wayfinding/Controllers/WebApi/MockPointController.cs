﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Wayfinding.UnitTest;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class MockPointController : IDbPointController
    {
        private List<PointOfInterest> MockPoints = MockDatabase.GetInstance().PointOfInterest;

        public IHttpActionResult AddPoint(AddPoint vm)
        {
            var point = new PointOfInterest
            {
                Name = vm.Name,
                NodeID = vm.NodeID,
                Size = vm.Size,
                Description = vm.Description,
                Icon = vm.Icon,
                Map = vm.Map
            };
            MockPoints.Add(point);
            return null;
        }

        public IHttpActionResult GetPoints(int MapID)
        {
            throw new NotImplementedException();
        }

        public IHttpActionResult RemovePoint(int ID)
        {
            PointOfInterest p = MockPoints.Where(x => x.NodeID == ID).First();
            MockPoints.Remove(p);
            return null;
        }

        public IHttpActionResult UpdatePoint(UpdatePoint vm)
        {
            PointOfInterest p = MockPoints.Where(x => x.NodeID == vm.ID).First();
            p.Size = (int)vm.Size;
            return null;
        }
    }
}