﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public interface IDbRoadController
    {
        IHttpActionResult GetRoads(int MapID);
        IHttpActionResult GetPath(int FirstNode, int SecondNode, int MapID);
        IHttpActionResult AddRoad(AddRoad vm);
        IHttpActionResult RemoveRoad(int ID);
        IHttpActionResult UpdateRoad(UpdateRoad vm);
    }
}
