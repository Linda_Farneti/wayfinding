﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wayfinding.Services;
using Wayfinding.ViewModels;
using Wayfinding.ViewModels.Node;

namespace Wayfinding.Controllers.WebApi
{
    public class NodeController : ApiController, IDbNodeController
    {
        private NodeQueryService nqs;
        private NodeService ns;

        public NodeController()
        {
            nqs = new NodeQueryService();
            ns = new NodeService();
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetNodes(int MapID)
        {
            return Ok(this.nqs.GetNodes(MapID));
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult AddNode(AddNode vm)
        {
            return Ok(this.ns.AddNode(vm));
        }

        public IHttpActionResult RemoveNode(int ID)
        {
            return Ok(this.ns.RemoveNode(ID));
        }

        [System.Web.Http.HttpPost]
       // [System.Web.Http.Route("UpdateNodePosition")]
        public IHttpActionResult UpdateNodePosition(UpdateNode vm)
        {
            return Ok(this.ns.UpdateNodePosition(vm));
        }
    }
}
