﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public interface IDbPointController
    {
        IHttpActionResult GetPoints(int MapID);

        IHttpActionResult AddPoint(AddPoint vm);

        IHttpActionResult RemovePoint(int ID);

        IHttpActionResult UpdatePoint(UpdatePoint vm);
    }
}
