﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class SettingsController : ApiController
    {
        [System.Web.Http.HttpPost]
        public IHttpActionResult ChangeColor(ChangeColor vm)
        {
            var database = new WayFindingUniBOEntities1();
            var e = vm.Element;
            var c = vm.Color;
            ColorSettings toUpdate = database.ColorSettings.Where(x=> x.Element == vm.Element).First();
            toUpdate.Color = ""+vm.Color;
            database.Entry(toUpdate).State = System.Data.Entity.EntityState.Modified;
            database.SaveChanges();
            return Ok(vm);
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetColors()
        {
            var database = new WayFindingUniBOEntities1();
            var colors = database.ColorSettings.Select(x => x.Color).ToArray();
            return Ok(colors);
        }
    }
}