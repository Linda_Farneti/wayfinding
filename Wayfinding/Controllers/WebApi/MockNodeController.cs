﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Wayfinding.UnitTest;
using Wayfinding.ViewModels;
using Wayfinding.ViewModels.Node;

namespace Wayfinding.Controllers.WebApi
{
    public class MockNodeController : IDbNodeController
    {
        List<Node> MockNodes = MockDatabase.GetInstance().Node;

        public IHttpActionResult AddNode(AddNode ian)
        {
            var node = new Node
            {
                Latitude = ian.Latitude,
                Longitude = ian.Longitude,
                Map = ian.Map,
            };
            if(MockNodes.Count() == 0)
            {
                node.ID = 1;
            }
            else
            {
                node.ID = MockNodes.Select(x => x.ID).Max() + 1;
            }
            MockNodes.Add(node);
            return null;
        }

        public IHttpActionResult GetNodes(int MapID)
        {
            //String a = "Mock get nodes";
            return null;
        }

        public IHttpActionResult RemoveNode(int ID)
        {
            Node n = MockNodes.Where(x => x.ID == ID).First();
            MockNodes.Remove(n);
            return null;
        }

        public IHttpActionResult UpdateNodePosition(UpdateNode iun)
        {
            Node n = MockNodes.Where(x => x.ID == iun.ID).First();
            n.Latitude = iun.Latitude;
            n.Longitude = iun.Longitude;
            return null;
        }
    }
}