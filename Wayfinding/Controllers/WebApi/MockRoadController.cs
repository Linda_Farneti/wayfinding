﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Wayfinding.UnitTest;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class MockRoadController : IDbRoadController
    {

        private List<Road> MockRoads = MockDatabase.GetInstance().Road;

        public IHttpActionResult AddRoad(AddRoad vm)
        {
            var road = new Road
            {
                Length = vm.Length,
                StartNodeID = vm.StartNodeID,
                EndNodeID = vm.EndNodeID,
                RoadSize = vm.Size,
                Map = vm.Map
            };
            if (MockRoads.Count() == 0)
            {
                road.ID = 1;
            }
            else
            {
                road.ID = MockRoads.Select(x => x.ID).Max() + 1;
            }
            MockRoads.Add(road);
            return null;
        }

        public IHttpActionResult GetPath(int FirstNode, int SecondNode, int MapID)
        {
            return null;
        }

        public IHttpActionResult GetRoads(int MapID)
        {
            return null;
        }

        public IHttpActionResult RemoveRoad(int ID)
        {
            Road r = MockRoads.Where(x => x.ID == ID).First();
            MockRoads.Remove(r);
            return null;
        }

        public IHttpActionResult UpdateRoad(UpdateRoad vm)
        {
            Road r = MockRoads.Where(x => x.ID == vm.ID).First();
            r.Length = (float) vm.NewLength;
            r.RoadSize = (int) vm.NewSize;
            return null;
        }
    }


}
