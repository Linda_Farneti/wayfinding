﻿using EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wayfinding.Services;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class UserController : ApiController
    {
        [System.Web.Http.HttpPost]
        public IHttpActionResult Login(Login vm)
        {
            var database = new WayFindingUniBOEntities1();
            var a = vm.Username;
            var b = vm.Password;
            string password;
            try
            {
                password = database.UserTable.Where(y => y.Nome.Equals(vm.Username)).Select(x => x.Password).First().ToString();
            }
            catch (System.InvalidOperationException)
            {
                password = null;
            }

            if (password == null)
            {
                return Ok("incorrectUser");
            }
            else
            {
                if (password.Equals(vm.Password))
                {
                    return Ok(vm.Username);
                }
                else
                {
                    return Ok("incorrectPassword");
                }
            }
            
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult Signup(SignupViewModel vm)
        {
            var database = new WayFindingUniBOEntities1();
            var a = vm.Username;
            var b = vm.Password;

            var user = new UserTable
            {
                Nome = vm.Username,
                Password = vm.Password
            };

            database.UserTable.Add(user);
            database.SaveChanges();
            return Ok();

        }
    }
}