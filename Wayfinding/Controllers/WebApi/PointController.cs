﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wayfinding.Services;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers.WebApi
{
    public class PointController : ApiController, IDbPointController
    {
        private PoiQueryService pqs;
        private PoiService ps;

        public PointController()
        {
            this.pqs = new PoiQueryService();
            this.ps = new PoiService();
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetPoints(int MapID)
        {
            return Ok(this.pqs.GetPoints(MapID));
        }


        [System.Web.Http.HttpPost]
        public IHttpActionResult AddPoint(AddPoint vm)
        {
            return Ok(this.ps.AddPoint(vm));
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult RemovePoint(int ID)
        {
            return Ok(this.ps.RemovePoint(ID));
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult UpdatePoint(UpdatePoint vm)
        {
            if (vm.Size.HasValue)
            {
                return Ok(this.ps.UpdatePointSize(vm.ID, vm.Size.Value));
            }
            else
            {
                return Ok(this.ps.UpdatePointDescription(vm.ID, vm.Desc));
            }
        }
    }
}
