﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wayfinding.ViewModels;

namespace Wayfinding.Controllers
{
    public class EditorController : Controller
    {
        // GET: Editor
        public ActionResult Index(int ID)
        {
            var vm = new EditorViewModel
            {
                ID = ID
            };
            return View("Index", vm);
        }
    }

  
}