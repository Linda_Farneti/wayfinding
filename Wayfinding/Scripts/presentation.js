 $(document).ready(function(){
    var check="";
    $("#loginpanel").hide();
    $("#signuppanel").hide();
    $("#incorrectpassword").hide();
    $("#incorrectuser").hide();

    $("#logincommand").click(() =>{
        $("#userchoose").hide();
        $("#loginpanel").fadeIn();
    });

    $("#signupcommand").click(() =>{
        $("#userchoose").hide();
        $("#signuppanel").fadeIn();
    });

    $("#backlogin").click(() => {
        $("#userchoose").fadeIn();
        $("#loginpanel").hide();
    });

    $("#backsignup").click(() => {
        $("#userchoose").fadeIn();
        $("#signuppanel").hide();
    });

    $("#username").click(function(){
        $(this).removeAttr("style");
        $("#incorrectuser").hide();
    })

    $("#password").click(function(){
        $(this).removeAttr("style");
        $("#incorrectpassword").hide();
    })

    $("#login").click(()=>{
        //alert("Log in as : "+ $("#username").val() + " -> " + $("#password").val());
        $.ajax({
			type: "POST",
            url: "http://localhost/Wayfinding/Api/User/Login",
            contentType: "application/json",
			data: JSON.stringify({ Username:  $("#username").val(), Password: encrypt($("#password").val())}),
            complete: (i) => {
                check = i.responseText;
                check = check.replace("\"","");
                check = check.replace("\"","");
                if(check.toString().endsWith("User")){
                    $("#username").css("border-color", "red");
                    $("#incorrectuser").fadeIn();
                }else if(check.toString().endsWith("Password")){
                    $("#password").css("border-color", "red");
                    $("#incorrectpassword").fadeIn();
                }else{
                    window.location.href = ("http://localhost/Wayfinding/GetStarted?User=" + check);
                }
			},
			dataType: "json"
        });
    });

    $("#passwordSet, #passwordRepeat").click(()=>{
        $("#passwordSet, #passwordRepeat").removeAttr("style");
    });

    $("#signup").click(()=>{
        let p = $("#passwordSet").val();
        let p2 = $("#passwordRepeat").val();
        if(p === p2){
            var password = encrypt(p);
            $.ajax({
                type: "POST",
                url: "http://localhost/Wayfinding/Api/User/Signup",
                contentType: "application/json",
                data: JSON.stringify({ Username:  $("#usernameSet").val(), Password: password}),
                complete: (i) => {
                    
                },
                dataType: "json"
            });
        }else{
            $("#passwordSet").css("border-color", "red");
            $("#passwordRepeat").css("border-color", "red");
        }
    });

    function encrypt(p){
        var vet = p.split('');
        var ar = new Array();
        vet.forEach(x => {
            ar.push(x.charCodeAt() + 5);
        });
        vet = new Array();
        ar.forEach(x=> {
            vet.push(String.fromCharCode(x));
        });
        vet = vet.toString();
        vet = vet.split(",").join("");  
        return vet;
    }
});