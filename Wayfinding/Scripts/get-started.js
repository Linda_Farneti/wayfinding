﻿$(document).ready(function () {
    var add = true;
    var remove = true;
    var icons = new Array();
    var selectedIcons = new Array();
    var nodec = "grey", nbc = "grey", roadc="white", rssc="blue", nssc="blue";
    var colorsetting;
    $("#managemaps").hide();
    $("#addicon").hide();
    $("#removeicon").hide();

    $(".color > button").each(function() {
        $(this).css("background-color", $(this).attr('class'));
    });

    $("#option1 > .color  button").click(function(){
        $(" #option1 > .color button").each(function() {
            $(this).css("border", "transparent");
        });
        $(this).css("border", "red solid 3px");
        nodec = ""+$(this).attr('class');
        console.log("Node color: " + nodec);
    });

    $("#option2 > .color  button").click(function(){
        $(" #option2 > .color button").each(function() {
            $(this).css("border", "transparent");
        });
        $(this).css("border", "red solid 3px");
        nbc = $(this).attr('class');
        console.log("Node border color: " + nbc);
    });

    $("#option3 > .color  button").click(function(){
        $(" #option3 > .color button").each(function() {
            $(this).css("border", "transparent");
        });
        $(this).css("border", "red solid 3px");
        roadc = $(this).attr('class');
        console.log("Road color: " + roadc);
    });

    $("#option4 > .color  button").click(function(){
        $(" #option4 > .color button").each(function() {
            $(this).css("border", "transparent");
        });
        $(this).css("border", "red solid 3px");
        rssc = $(this).attr('class');
        console.log("Road Selected Style color: " + rssc);
    });

    $("#option5 > .color  button").click(function(){
        $(" #option5 > .color button").each(function() {
            $(this).css("border", "transparent");
        });
        $(this).css("border", "red solid 3px");
        nssc = $(this).attr('class');
        console.log("Node selected style color: " + nssc);
    });
   
    $("#savechanges").click(()=>{
        colorsetting = [["Circle", nodec], ["CircleEdge",nbc],  ["Road",roadc],  ["RoadSelectionStyle",rssc], ["NodeSelectionStyle",nssc]];
        for(let i = 0; i < colorsetting.length; i++){
            $.ajax({
                type: "POST",
                url: "http://localhost/Wayfinding/Api/Settings/ChangeColor",
                contentType: "application/json",
                data: JSON.stringify({ Element: colorsetting[i][0], Color: colorsetting[i][1]}),
                dataType: "json"
            });
        }
       /* $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Settings/ChangeColor",
            contentType: "application/json",
            data: JSON.stringify({ Element: "Circle", Color: nodec}),
            dataType: "json"
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Settings/ChangeColor",
            contentType: "application/json",
            data: JSON.stringify({ Element: "CircleEdge", Color: nbc}),
            dataType: "json"
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Settings/ChangeColor",
            contentType: "application/json",
            data: JSON.stringify({ Element: "Road", Color: roadc}),
            dataType: "json"
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Settings/ChangeColor",
            contentType: "application/json",
            data: JSON.stringify({ Element: "RoadSelectionStyle", Color: rssc}),
            dataType: "json"
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/Wayfinding/Api/Settings/ChangeColor",
            contentType: "application/json",
            data: JSON.stringify({ Element: "NodeSelectionStyle", Color: nssc}),
            dataType: "json"
        });*/
    });
     //$("#load_panel").hide();

    $.ajax({
        type: "GET",
        url: "http://localhost/Wayfinding/Api/Map/GetMaps?User="+User,
        contentType: "application/json",
        success: (data) => {
            $("#load_gif").hide();
            var d = new Array(data);
            for (let i = 0; i < d[0].length; i++) {
                $("#map_name").append($('<option></option>').attr('value', d[0][i].Id).text(d[0][i].Name));
            };
        },
        dataType: "json"
    });
    loadIcon();
    function loadIcon(){
        $.ajax({
            type: "GET",
            url: "http://localhost/Wayfinding/Api/Icon/GetIcons",
            contentType: "application/json", 
            success: (i)=>{
                let a = new Array(i);
                for(let index = 0; index < a[0].length; index++){
                    icons.push({"ID":a[0][index].Id, "Name": a[0][index].Name});
                }
                icons.forEach(i => {
                    var r = $('<input/>').attr({
                        class: "icons",
                        type: "image",
                        src: "Images/" + i.Name + ".svg",
                        name: i,
                        width: "40",
                        height: "40",
                        title: i.ID
                    });        
                    $(r).on("click", ()=>{
                        let id = r.attr("title");
                        console.log(r.attr("title")+ "---" + r.attr("name"));
                        if(selectedIcons.indexOf(id) >= 0){
                            selectedIcons.pop(id);
                            $(r).removeAttr("style");
                        }else{
                            selectedIcons.push(id);
                            $(r).css("background", "green");
                        }
                    });
                    $("#allicons").append(r);
                });
                dataType: "json"
            }
        });
    }

    $.ajax({ 
        type:"GET",
        url: "http://localhost/Wayfinding/Api/Map/GetMaps?User="+User,
        contentType: "application/json",
        success: (data) => {
            var m = new Array(data);
            for(let i = 0; i < m[0].length; i++){
                var t = $('<label></label>').attr('value', m[0][i].id).text(m[0][i].Name);
                var t2 =  $('<label></label>').attr('value', m[0][i].id).text(" ");
                var d = $('<div></div>');
                var d2 = $('<div></div>');
                d2.attr('id', m[0][i].Id);
                d.append(t).append(t2).append(d2);
                d.click(() => {
                    window.location.href = "http://localhost/Wayfinding/Editor?ID=" + Number( m[0][i].Id);
                });
                $("#managemaps").append(d);
                var mymap = L.map(''+m[0][i].Id).setView([m[0][i].Latitude, m[0][i].Longitude], 10);
                L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                }).addTo(mymap);
            }
        }
    })

    $("#add_icon_btn").change(() => {
        console.log($("#toUpload").val());
        let a = $("#add_icon_btn").val();
        console.log(a);
        a = a.substring(a.lastIndexOf('\\') + 1);
        a = '/Wayfinding/EditorClient/src/view/images/' + a;
        $("#toUpload").attr("src", a);

    });

    $("#upload_icon").click(() => {
        $("#add_icon").fadeToggle();
    });

    $("#settings").click(() => {
        $("#title").hide();
        $("#choose").fadeOut();
        $("#icons").fadeIn();
    });

    $("#newmap").click(() => {
        window.location.href = "http://localhost/Wayfinding/NewMap?User="+User;
    });

    $("#maps").click(() => {
        $("#choose").fadeOut();
        $("#managemaps").fadeIn();
    });

    $("#ok").click(() => {
        $("#allicons").remove(".icons");
        loadIcon();
        //window.location.href = "http://localhost/Wayfinding/Editor?ID=" + Number($("#map_name").val());
    });

    $("#add").click(() => {
        $("#addicon").slideToggle();
        if(add){
            $("#add > img").attr("src", "Content/WebImages/arrowup.svg");
        }else{
            $("#add > img").attr("src", "Content/WebImages/arrowdown.svg");
        }
        add = !add;
    });

    $("#remove").click(() => {
        $("#removeicon").slideToggle();
        if(remove){
            $("#remove > img").attr("src", "Content/WebImages/arrowup.svg");
        }else{
            $("#remove > img").attr("src", "Content/WebImages/arrowdown.svg");
        }
        remove = !remove;
    });

    $("#removeselected").click(() => {
        console.log(selectedIcons.toString());
        selectedIcons.forEach(id =>{
            $("[title=" +id+"]").fadeOut();
            $.ajax({
                type: "GET",
                url: "http://localhost/Wayfinding/Api/Icon/RemoveIcon?ID="+id,
                contentType: "application/json",
                dataType: "json"
            });
        } )
        
    
    });
});

