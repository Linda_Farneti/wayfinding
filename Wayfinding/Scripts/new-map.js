﻿$(document).ready(function () {
	/* var mapOpt = {
         center: new google.maps.LatLng(51.5, -0.12),
         zoom: 10,
         mapTypeId: google.maps.MapTypeId.HYBRID
     }
 
     var map = new google.maps.Map(document.getElementById("map"), mapOptions);*/
	 var a = true;
	 var b = true;
	var mymap = L.map('map').setView([42.5, 12], 6);
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(mymap);

	mymap.on("zoom", (event) => {
		$("#latitude").val(mymap.getCenter().lat);
		$("#longitude").val(mymap.getCenter().lng);
		$("#zoom").val(mymap.getZoom());
	});

	mymap.on("dragend", () => {
		$("#latitude").val(mymap.getCenter().lat);
		$("#longitude").val(mymap.getCenter().lng);
	})


	$("#savemap").on("mouseenter", ()=>{
		$("#savemap label").css("opacity", "1");
		$("#savemap label").css("transform", "translate(0px)");
	});
	$("#savemap").on("mouseleave", ()=>{
		$("#savemap label").css("opacity", "0");
		$("#savemap label").css("transform", "translate(-70px)");
	});

	$("#seedetails").click(() => {
		if(a){
			$("#options").css("transform", "translate(0px)");
			$("#map").css("margin-top", "260px");
			$("#seedetails").text("<< Details");
		}else{
			$("#options").css("transform", "translate(-1120px)");
			$("#map").css("margin-top", "155px");
			$("#seedetails").text("Details >>");
		}
		a = !a;
	})

	$("#savelabel").click(() => {
		if(b){
			$("#save_panel").css("transform", "translate(320px)");
			$("#savelabel").text("<< Done");			
		}else{
			$("#save_panel").css("transform", "translate(-620px)");
			$("#savelabel").text("Done >>");
		}
		b = !b;
	})
    $("#savemap").click(() => {
		let name = $("#name").val();
		let latitude = Number($("#latitude").val());
		let longitude = Number($("#longitude").val());
		let zoom = Number($("#zoom").val());
		$.ajax({
			type: "POST",
            url: "http://localhost/Wayfinding/Api/Map/AddMap",
            contentType: "application/json",
			data: JSON.stringify({ Name: name, Latitude: latitude, Longitude: longitude, Zoom: zoom, Username: User }),
            complete: (i) => {
                alert("ID=" + i.responseText);
				window.location.href = "http://localhost/Wayfinding/Editor?ID="+ Number(i.responseText);
			},
			dataType: "json"
		});
	});
});